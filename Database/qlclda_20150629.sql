-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: qlclda
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_project` int(5) NOT NULL,
  `who_activity` varchar(100) NOT NULL,
  `date_update` datetime NOT NULL,
  `content_update` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_project` (`id_project`),
  CONSTRAINT `activity_log_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_estimate`
--

DROP TABLE IF EXISTS `app_estimate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_estimate` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(2000) NOT NULL,
  `detail` varchar(200) NOT NULL,
  `create` varchar(20) NOT NULL,
  `is_disable` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bank_info`
--

DROP TABLE IF EXISTS `bank_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_pay` varchar(200) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `street` varchar(200) DEFAULT NULL,
  `district` varchar(200) DEFAULT NULL,
  `tel` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `swift_code` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalogue`
--

DROP TABLE IF EXISTS `catalogue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogue` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(2000) NOT NULL,
  `Detail` varchar(2000) NOT NULL,
  `create` varchar(200) NOT NULL,
  `is_disable` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `company_cd` varchar(50) NOT NULL,
  `company` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `representative` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `end_project`
--

DROP TABLE IF EXISTS `end_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `end_project` (
  `project_id` int(5) NOT NULL,
  `difficulty` longtext,
  `advantage` longtext,
  `risk` longtext,
  `experience` longtext,
  `is_fail` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  CONSTRAINT `fk_end_project_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estimate_library`
--

DROP TABLE IF EXISTS `estimate_library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estimate_library` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_project` int(10) NOT NULL,
  `id_type_app` int(10) NOT NULL,
  `id_type_project` int(10) NOT NULL,
  `id_function_da` int(10) NOT NULL,
  `LOC` int(200) NOT NULL,
  `external` varchar(2000) NOT NULL,
  `remark` varchar(2000) NOT NULL,
  `tags` varchar(2000) NOT NULL,
  `created` varchar(200) NOT NULL,
  `mod_date` varchar(200) NOT NULL,
  `order` int(10) NOT NULL,
  `is_disabled` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `function_da`
--

DROP TABLE IF EXISTS `function_da`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `function_da` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(2000) NOT NULL,
  `detail` varchar(2000) NOT NULL,
  `created` varchar(20) NOT NULL,
  `order` int(10) NOT NULL,
  `is_disabled` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level` (
  `id` int(4) NOT NULL,
  `name` varchar(2) NOT NULL,
  `group` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `position` (
  `id` smallint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_customer` int(5) NOT NULL,
  `id_type_project` int(3) NOT NULL,
  `name_cd` varchar(50) NOT NULL,
  `name_project` varchar(200) NOT NULL,
  `expected_start_date` date NOT NULL,
  `expected_end_date` date NOT NULL,
  `actual_start_date` date DEFAULT NULL,
  `actual_end_date` date DEFAULT NULL,
  `technology` text,
  `purpose` varchar(200) NOT NULL,
  `estimator` varchar(100) DEFAULT NULL,
  `scale_estimator` int(3) DEFAULT NULL,
  `contract_value` int(4) DEFAULT NULL,
  `pm` int(6) NOT NULL,
  `brse` int(6) DEFAULT NULL,
  `current_cost` int(4) DEFAULT NULL,
  `estimated_cost` int(4) DEFAULT NULL,
  `software` varchar(200) DEFAULT NULL,
  `hardware` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_customer` (`id_customer`),
  KEY `id_type_project` (`id_type_project`),
  KEY `fk_project_staff1_idx` (`pm`),
  KEY `project_ibfk_4` (`brse`),
  CONSTRAINT `fk_project_staff1` FOREIGN KEY (`pm`) REFERENCES `staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id`),
  CONSTRAINT `project_ibfk_2` FOREIGN KEY (`id_type_project`) REFERENCES `type_project` (`id`),
  CONSTRAINT `project_ibfk_3` FOREIGN KEY (`brse`) REFERENCES `staff` (`id`),
  CONSTRAINT `project_ibfk_4` FOREIGN KEY (`brse`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_functione`
--

DROP TABLE IF EXISTS `project_functione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_functione` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_project` int(10) NOT NULL,
  `id_task_list` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_skill`
--

DROP TABLE IF EXISTS `project_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_skill` (
  `project_id` int(5) NOT NULL,
  `skill_id` int(4) NOT NULL,
  PRIMARY KEY (`project_id`,`skill_id`),
  KEY `skill_id` (`skill_id`),
  CONSTRAINT `project_skill_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `project_skill_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_type_app`
--

DROP TABLE IF EXISTS `project_type_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_type_app` (
  `project_id` int(5) NOT NULL,
  `type_app_id` smallint(2) NOT NULL,
  PRIMARY KEY (`project_id`,`type_app_id`),
  KEY `type_app_id` (`type_app_id`),
  CONSTRAINT `project_type_app_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `project_type_app_ibfk_2` FOREIGN KEY (`type_app_id`) REFERENCES `type_app` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quality`
--

DROP TABLE IF EXISTS `quality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality` (
  `project_id` int(5) NOT NULL,
  `number_kloc` int(3) DEFAULT NULL,
  `number_bug_of_team` int(3) DEFAULT NULL,
  `number_bug_of_team_in_kloc` int(2) DEFAULT NULL,
  `number_bug_of_customer` int(3) DEFAULT NULL,
  `number_bug_of_customer_int_kloc` int(2) DEFAULT NULL,
  `number_test_case` int(3) DEFAULT NULL,
  `human_resource` int(3) DEFAULT NULL,
  `time_complete` int(3) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  CONSTRAINT `fk_quality_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `staff_id` int(6) NOT NULL,
  `project_id` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `resource_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  CONSTRAINT `resource_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `type_skill_id` smallint(2) NOT NULL,
  `parent_skill_id` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_skill_id` (`type_skill_id`),
  CONSTRAINT `skill_ibfk_1` FOREIGN KEY (`type_skill_id`) REFERENCES `type_skill` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `position_id` smallint(2) NOT NULL,
  `display_order` smallint(2) DEFAULT '10',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `position_id` (`position_id`),
  CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff_of_skill`
--

DROP TABLE IF EXISTS `staff_of_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_of_skill` (
  `staff_id` int(6) NOT NULL,
  `skill_id` int(4) NOT NULL,
  `level_id` int(4) DEFAULT NULL,
  `num_exp` int(11) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`staff_id`,`skill_id`),
  KEY `skill_id` (`skill_id`),
  CONSTRAINT `staff_of_skill_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  CONSTRAINT `staff_of_skill_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task_list`
--

DROP TABLE IF EXISTS `task_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_list` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `detail` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `created` varchar(200) CHARACTER SET utf8 NOT NULL,
  `order` int(10) NOT NULL,
  `is_disabled` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task_list_catatogue`
--

DROP TABLE IF EXISTS `task_list_catatogue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_list_catatogue` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_task_list_estimate` int(10) NOT NULL,
  `id_catalogue` int(10) NOT NULL,
  `day` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task_list_estimate`
--

DROP TABLE IF EXISTS `task_list_estimate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_list_estimate` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `category` int(10) NOT NULL,
  `id_app_estimate` int(10) NOT NULL,
  `oss` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `created` varchar(200) CHARACTER SET utf8 NOT NULL,
  `order` int(10) NOT NULL,
  `is_disabled` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `workplace` varchar(100) NOT NULL,
  `technique` varchar(200) DEFAULT NULL,
  `display_order` smallint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `team_project`
--

DROP TABLE IF EXISTS `team_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_project` (
  `id_team` int(3) NOT NULL,
  `id_project` int(5) NOT NULL,
  PRIMARY KEY (`id_team`,`id_project`),
  KEY `id_project` (`id_project`),
  CONSTRAINT `team_project_ibfk_1` FOREIGN KEY (`id_team`) REFERENCES `team` (`id`),
  CONSTRAINT `team_project_ibfk_2` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `team_staff`
--

DROP TABLE IF EXISTS `team_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_staff` (
  `id_team` int(3) NOT NULL,
  `id_staff` int(5) NOT NULL,
  `leader` tinyint(1) DEFAULT NULL,
  `is_main_team` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_team`,`id_staff`),
  KEY `id_staff` (`id_staff`),
  CONSTRAINT `team_staff_ibfk_1` FOREIGN KEY (`id_team`) REFERENCES `team` (`id`),
  CONSTRAINT `team_staff_ibfk_2` FOREIGN KEY (`id_staff`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_app`
--

DROP TABLE IF EXISTS `type_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_app` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_project`
--

DROP TABLE IF EXISTS `type_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_project` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_skill`
--

DROP TABLE IF EXISTS `type_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_skill` (
  `id` smallint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(45) NOT NULL DEFAULT '123456',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `works`
--

DROP TABLE IF EXISTS `works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `works` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `resource_id` int(8) NOT NULL,
  `work_day` date DEFAULT NULL,
  `percent` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `works_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2571 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-29 14:09:38
