CREATE DATABASE  IF NOT EXISTS `qlclda` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `qlclda`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: qlclda
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_project` int(5) NOT NULL,
  `who_activity` varchar(100) NOT NULL,
  `date_update` datetime NOT NULL,
  `content_update` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_project` (`id_project`),
  CONSTRAINT `activity_log_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (34,1,'','2014-05-21 15:52:16','[Cập nhật] purpose: \"hihihi\" thành \"hihihihaha\"'),(35,5,'','2014-05-21 04:09:54','[Cập nhật] purpose: \"ssssssssssss\" thành \"Bán điện thoại\"'),(36,28,'','2014-05-21 05:03:47','[Thêm mới] ID: 0 - Dân trí');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_info`
--

DROP TABLE IF EXISTS `bank_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_pay` varchar(200) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `street` varchar(200) DEFAULT NULL,
  `district` varchar(200) DEFAULT NULL,
  `tel` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `swift_code` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_info`
--

LOCK TABLES `bank_info` WRITE;
/*!40000 ALTER TABLE `bank_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `company_cd` varchar(50) NOT NULL,
  `company` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `representative` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'FPT','FPT Software','Cầu Giấy','Nguyễn Văn A','abc@fpt.vn','nva','123213123',NULL,NULL),(2,'PICO','Điện máy PICO','76 Nguyễn Trãi','Trần Anh B','bbbb@pico.vn','bbbb','24434546',NULL,NULL);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `end_project`
--

DROP TABLE IF EXISTS `end_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `end_project` (
  `project_id` int(5) NOT NULL,
  `difficulty` longtext,
  `advantage` longtext,
  `risk` longtext,
  `experience` longtext,
  `is_fail` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  CONSTRAINT `fk_end_project_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `end_project`
--

LOCK TABLES `end_project` WRITE;
/*!40000 ALTER TABLE `end_project` DISABLE KEYS */;
INSERT INTO `end_project` VALUES (5,'<div align=\"left\"><ul><li><i>Hehe</i></li><li><i>Hoho</i></li><li><i>Haha</i></li></ul></div>','<ol><li>Test</li><li>Test2</li><li>Test3<br></li></ol>','<ul><li><font face=\"trebuchet ms\"><b>Test 4</b></font></li><li><font face=\"trebuchet ms\"><b>Test 7<br></b></font></li></ul>','<ul><li><font face=\"impact\">Nói chung</font></li><li><font face=\"impact\">là không</font></li><li><font face=\"impact\">có gì </font></li></ul>',0),(9,'Test<br>','Test','Test','Test',1);
/*!40000 ALTER TABLE `end_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_customer` int(5) NOT NULL,
  `id_type_project` int(3) NOT NULL,
  `name_cd` varchar(50) DEFAULT NULL,
  `name_project` varchar(200) NOT NULL,
  `expected_start_date` date NOT NULL,
  `expected_end_date` date NOT NULL,
  `actual_start_date` date DEFAULT NULL,
  `actual_end_date` date DEFAULT NULL,
  `technology` text,
  `purpose` varchar(200) NOT NULL,
  `estimator` varchar(100) DEFAULT NULL,
  `scale_estimator` int(3) DEFAULT NULL,
  `contract_value` int(4) DEFAULT NULL,
  `pm` int(6) DEFAULT NULL,
  `brse` varchar(100) DEFAULT NULL,
  `current_cost` int(4) DEFAULT NULL,
  `estimated_cost` int(4) DEFAULT NULL,
  `software` varchar(200) DEFAULT NULL,
  `hardware` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_customer` (`id_customer`),
  KEY `id_type_project` (`id_type_project`),
  KEY `fk_project_staff1_idx` (`pm`),
  CONSTRAINT `fk_project_staff1` FOREIGN KEY (`pm`) REFERENCES `staff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id`),
  CONSTRAINT `project_ibfk_2` FOREIGN KEY (`id_type_project`) REFERENCES `type_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,1,2,'Demo','Demo GMO','2014-05-02','2014-05-05',NULL,NULL,'C#','hihihihaha','Dat 0923',3,100,3,'Suong 067',10,100,'213','32398'),(5,1,1,'FPT A1','FPT Mobile F22','2014-05-02','2014-06-30',NULL,NULL,'C#','Bán điện thoại','Dat 09',3,500,1,'Suong 06',10,100,NULL,NULL),(6,1,1,'PICO','PICO Website','2014-02-01','2014-03-05',NULL,NULL,'C#','ssssssssssss','Dat 09',12,8000,1,'Suong 06',10,100,NULL,NULL),(7,1,1,'Lingo','Lingo Card','2013-08-08','2013-11-20',NULL,NULL,'C#','cấp thẻ','Dat 09',15,250,NULL,'Suong 06',10,100,NULL,NULL),(8,1,2,'FP','Flappy Bird','2014-05-02','2014-05-10',NULL,NULL,'C#','hê hê','Dat 09',14,750,1,'Suong 06',10,100,NULL,'huhu'),(9,1,4,'Misa','Phần mềm Kế toán Misa','2014-05-02','2014-05-29',NULL,NULL,'C#','Kế toán','Dat 09',7,1500,6,'Suong 06',10,100,NULL,NULL),(10,1,1,'Amis','Phần mềm QLNS','2014-05-02','2014-05-25','2014-05-02','2014-05-20','C#','ssssssssssss','Dat 09',5,434,1,'Suong 06',10,100,NULL,NULL),(11,1,1,'CT5','Đường cao tốc 5','2014-05-02','2014-09-23',NULL,NULL,'C#','ssssssssssss','Dat 09',8,653,1,'Suong 06',10,100,NULL,NULL),(13,1,1,'KB','KGB Online','2014-05-02','2014-07-23','2014-05-02','2014-07-20','C#','ssssssssssss','Dat 09',89,100,1,'Suong 06',10,100,NULL,NULL),(14,1,1,'AA','AAA','2014-05-02','2014-05-02',NULL,NULL,'C#','ssssssssssss','Dat 09',67,546,1,'Suong 06',10,100,NULL,NULL),(16,1,2,'FPT B2','FPT Software 1','2014-05-01','2014-05-22','2014-05-08',NULL,'C#','Chơi game','Không có',70,10000,2,'Không có',10000,10000,'No','No'),(17,1,1,'NONO','NÔ Body abc','2014-05-01','2014-05-14',NULL,NULL,NULL,'Không cóa',NULL,5,NULL,5,NULL,NULL,NULL,NULL,NULL),(27,1,2,'AA','Tết Công gô','2014-05-08','2014-05-30',NULL,NULL,NULL,'Hihi',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),(28,1,1,'DT','Dân trí','2014-05-08','2014-05-30',NULL,NULL,NULL,'Làm báo',NULL,NULL,NULL,10,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quality`
--

DROP TABLE IF EXISTS `quality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality` (
  `project_id` int(5) NOT NULL,
  `number_kloc` int(3) DEFAULT NULL,
  `number_bug_of_team` int(3) DEFAULT NULL,
  `number_bug_of_team_in_kloc` int(2) DEFAULT NULL,
  `number_bug_of_customer` int(3) DEFAULT NULL,
  `number_bug_of_customer_int_kloc` int(2) DEFAULT NULL,
  `number_test_case` int(3) DEFAULT NULL,
  `human_resource` int(3) DEFAULT NULL,
  `time_complete` int(3) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  CONSTRAINT `fk_quality_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quality`
--

LOCK TABLES `quality` WRITE;
/*!40000 ALTER TABLE `quality` DISABLE KEYS */;
INSERT INTO `quality` VALUES (5,10,100,12,20,30,1000,20,4),(16,1,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `quality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `staff_id` int(6) NOT NULL,
  `project_id` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `resource_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  CONSTRAINT `resource_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
INSERT INTO `resource` VALUES (1,1,1),(3,1,5);
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `role` varchar(100) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'Phạm Văn Đông','Học việc',NULL,'dongsama91','1234567890'),(2,'Nguyễn Đức Hưởng','Lập trình viên',NULL,'huongnd','4124343'),(3,'Nguyễn Bá Thành','Lập trình viên',NULL,'thanhnb','567678'),(4,'Ngô Quỳnh Anh','Ca sĩ',NULL,'ngoquynhanh','354577'),(5,'Bích Phương Idol','Ca sĩ','abc@email.com','bpidol','43454657'),(6,'Minh Vương','Nhạc sĩ',NULL,'minhvuong','9868645'),(8,'Trần Phúc Hoàn','Học việc',NULL,'theboy_net','0984548557'),(9,'Cao Thái Sơn','Ca sĩ',NULL,'cts','5454656'),(10,'Tiên Cookie','Nhạc sĩ',NULL,'tiencookie','4546565623'),(11,'Dân chơi','Chơi bời',NULL,'dcccc','34343434'),(12,'Thành Vương','Nhạc sĩ',NULL,'thanhvuong','54567676'),(13,'Huy Tuấn','Nhạc sĩ',NULL,'hihihi','34948969'),(14,'Mỹ Linh','Ca sĩ',NULL,'mylinh','1322434343'),(16,'Hồng Nhung','Ca sĩ',NULL,'hongnhung','333545'),(18,'Thanh Lam','Ca sĩ',NULL,'thanhlam','334354'),(20,'Mai Khôi','Ca sĩ',NULL,'maikhoi','545656'),(21,'Linh Phi','Ca sĩ',NULL,'maikhoi','545656'),(22,'Đăng Khôi','Ca sĩ',NULL,'dangkhoi','34454656'),(23,'Lăng Hải','Thực tập',NULL,'langhai','34354544'),(24,'Đức Trí','Nhạc sĩ',NULL,'ductri','45454545'),(25,'Khởi My','Ca sĩ',NULL,'khoimy','34909823'),(26,'Ông Cao Thắng','Ca sĩ',NULL,'ongcaothang','343434087'),(27,'Trần Văn A','Lập trình viên',NULL,'sdsadsd','54565656');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `workplace` varchar(100) NOT NULL,
  `technique` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'BizSystem','HVT','C#'),(2,'R&D','HVT','Nghiên cứu'),(3,'Power','Nguyễn Ngọc Nại','PHP'),(5,'Oh yeah','HVT','Java');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_project`
--

DROP TABLE IF EXISTS `team_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_project` (
  `id_team` int(3) NOT NULL,
  `id_project` int(5) NOT NULL,
  PRIMARY KEY (`id_team`,`id_project`),
  KEY `id_project` (`id_project`),
  CONSTRAINT `team_project_ibfk_1` FOREIGN KEY (`id_team`) REFERENCES `team` (`id`),
  CONSTRAINT `team_project_ibfk_2` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_project`
--

LOCK TABLES `team_project` WRITE;
/*!40000 ALTER TABLE `team_project` DISABLE KEYS */;
INSERT INTO `team_project` VALUES (3,7),(5,7),(3,8),(5,8),(1,9),(5,9),(1,10),(3,10),(1,11),(3,11),(5,16),(1,17),(1,28),(5,28);
/*!40000 ALTER TABLE `team_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_staff`
--

DROP TABLE IF EXISTS `team_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_staff` (
  `id_team` int(3) NOT NULL,
  `id_staff` int(5) NOT NULL,
  `leader` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_team`,`id_staff`),
  KEY `id_staff` (`id_staff`),
  CONSTRAINT `team_staff_ibfk_1` FOREIGN KEY (`id_team`) REFERENCES `team` (`id`),
  CONSTRAINT `team_staff_ibfk_2` FOREIGN KEY (`id_staff`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_staff`
--

LOCK TABLES `team_staff` WRITE;
/*!40000 ALTER TABLE `team_staff` DISABLE KEYS */;
INSERT INTO `team_staff` VALUES (1,1,0),(1,2,1),(1,3,0),(2,1,1),(2,8,0),(3,3,0),(3,9,0),(3,10,1),(3,12,0),(3,14,1),(3,22,0),(5,12,0),(5,13,1),(5,14,0);
/*!40000 ALTER TABLE `team_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_project`
--

DROP TABLE IF EXISTS `type_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_project` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_project`
--

LOCK TABLES `type_project` WRITE;
/*!40000 ALTER TABLE `type_project` DISABLE KEYS */;
INSERT INTO `type_project` VALUES (1,'Gia công','Việt Nam'),(2,'Chuyển giao','Nhật Bản'),(3,'Đánh thuê','Mỹ'),(4,'Lâu dài','Việt Nam');
/*!40000 ALTER TABLE `type_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(45) NOT NULL DEFAULT '123456',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','123456'),(2,'dong','011191'),(3,'hoan','bcsok');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `works`
--

DROP TABLE IF EXISTS `works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `works` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `resource_id` int(8) NOT NULL,
  `work_day` date DEFAULT NULL,
  `percent` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `works_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `works`
--

LOCK TABLES `works` WRITE;
/*!40000 ALTER TABLE `works` DISABLE KEYS */;
INSERT INTO `works` VALUES (1,1,'2014-09-09',50),(2,1,'2014-09-10',30),(3,3,'2014-09-09',10);
/*!40000 ALTER TABLE `works` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-08  0:30:29
