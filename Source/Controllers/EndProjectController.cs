﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class EndProjectController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /EndProject/
        public ActionResult Index()
        {
            var endProject = _db.end_project.Include(e => e.project);
            return View(endProject.ToList());
        }

        // GET: /EndProject/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var endProject = _db.end_project.Find(id);
            if (endProject == null)
            {
                return HttpNotFound();
            }
            return View(endProject);
        }

        // GET: /EndProject/Create
        public ActionResult Create()
        {
            ViewBag.project_id = new SelectList(_db.projects, "id", "name_project");
            return View();
        }

        // POST: /EndProject/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="project_id,difficulty,advantage,risk,experience,is_fail")] end_project endProject)
        {
            if (ModelState.IsValid)
            {
                _db.end_project.Add(endProject);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.project_id = new SelectList(_db.projects, "id", "name_project", endProject.project_id);
            return View(endProject);
        }

        // GET: /EndProject/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var endProject = _db.end_project.Find(id);
            if (endProject == null)
            {
                return HttpNotFound();
            }
            ViewBag.project_id = new SelectList(_db.projects, "id", "name_cd", endProject.project_id);
            return View(endProject);
        }

        // POST: /EndProject/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="project_id,difficulty,advantage,risk,experience,is_fail")] end_project endProject)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(endProject).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.project_id = new SelectList(_db.projects, "id", "name_project", endProject.project_id);
            return View(endProject);
        }

        // GET: /EndProject/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var endProject = _db.end_project.Find(id);
            if (endProject == null)
            {
                return HttpNotFound();
            }
            return View(endProject);
        }

        // POST: /EndProject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var endProject = _db.end_project.Find(id);
            _db.end_project.Remove(endProject);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
