﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class TypeProjectController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /TypeProject/
        public ActionResult Index()
        {
            return View(_db.type_project.ToList());
        }

        // GET: /TypeProject/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeProject = _db.type_project.Find(id);
            if (typeProject == null)
            {
                return HttpNotFound();
            }
            return View(typeProject);
        }

        // GET: /TypeProject/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /TypeProject/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,name,country")] type_project typeProject)
        {
            if (ModelState.IsValid)
            {
                _db.type_project.Add(typeProject);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(typeProject);
        }

        // GET: /TypeProject/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeProject = _db.type_project.Find(id);
            if (typeProject == null)
            {
                return HttpNotFound();
            }
            return View(typeProject);
        }

        // POST: /TypeProject/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,name,country")] type_project typeProject)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(typeProject).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(typeProject);
        }

        // GET: /TypeProject/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var typeProject = _db.type_project.Find(id);
            if (typeProject == null)
            {
                return HttpNotFound();
            }
            return View(typeProject);
        }

        // POST: /TypeProject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var typeProject = _db.type_project.Find(id);
            _db.type_project.Remove(typeProject);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
