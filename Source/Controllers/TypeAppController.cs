﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;
using EntityState = System.Data.Entity.EntityState;

namespace QLCLDA.Views
{
    public class TypeAppController : Controller
    {
        private qlcldaEntities db = new qlcldaEntities();

        // GET: /TypeApp/
        public ActionResult Index()
        {
            return View(db.type_app.ToList());
        }

        // GET: /TypeApp/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            type_app type_app = db.type_app.Find(id);
            if (type_app == null)
            {
                return HttpNotFound();
            }
            return View(type_app);
        }

        // GET: /TypeApp/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /TypeApp/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,name,description")] type_app type_app)
        {
            if (ModelState.IsValid)
            {
                db.type_app.Add(type_app);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(type_app);
        }

        // GET: /TypeApp/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            type_app type_app = db.type_app.Find(id);
            if (type_app == null)
            {
                return HttpNotFound();
            }
            return View(type_app);
        }

        // POST: /TypeApp/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,name,description")] type_app type_app)
        {
            if (ModelState.IsValid)
            {
                db.Entry(type_app).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(type_app);
        }

        // GET: /TypeApp/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            type_app type_app = db.type_app.Find(id);
            if (type_app == null)
            {
                return HttpNotFound();
            }
            return View(type_app);
        }

        // POST: /TypeApp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            type_app type_app = db.type_app.Find(id);
            db.type_app.Remove(type_app);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
