﻿using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Chart;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class ReportController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SoLuongCharts()
        {
            var chartDraw = new ChartDraw();

            var q = from t in _db.teams
                    select new { t.name, value = t.projects.Count() };

            foreach (var item in q)
            {
                chartDraw.SoLuongCharts.Add(new PieChart
                {
                    name = item.name,
                    y = item.value
                });
            }

            return View(chartDraw);
        }

        //Biểu đồ số lượng dự án, doanh thu dự án theo năm
        public ActionResult DoanhThuLine()
        {
            var chartDraw = new ChartDraw();

            //Số lượng dự án theo năm
            var q = from p in _db.projects
                    group p by p.expected_start_date.Year into g
                    orderby g.Key
                    select g;

            foreach (var item in q)
            {
                chartDraw.NumberYear.Add(new ChartValue
                {
                    name = item.Key.ToString(CultureInfo.InvariantCulture),
                    value = item.Count()
                });
            }

            ViewBag.TCT = "Toàn công ty";

            //Doanh thu dự án theo năm
            var q1 = from p in _db.projects
                     where p.end_project != null
                     group p by p.expected_start_date.Year into g
                     orderby g.Key
                     select g;

            foreach (var item in q1)
            {
                chartDraw.RevenueYear.Add(new ChartValue
                {
                    name = item.Key.ToString(CultureInfo.InvariantCulture),
                    value = item.Sum(p => p.contract_value).GetValueOrDefault()
                });
            }

            return View(chartDraw);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}