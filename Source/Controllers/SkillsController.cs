﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;
using EntityState = System.Data.Entity.EntityState;

namespace QLCLDA.Controllers
{
    public class SkillsController : Controller
    {
        private qlcldaEntities db = new qlcldaEntities();

        public ActionResult Index()
        {
            var skills = db.skills.Include(s => s.type_skill);
            return View(skills.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            skill skill = db.skills.Find(id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            return View(skill);
        }

        public ActionResult Create()
        {
            ViewBag.type_skill_id = new SelectList(db.type_skill, "id", "name");

            var parentSkillId = db.skills.Where(s => s.parent_skill_id == null);
            ViewBag.parent_skill_id = new SelectList(parentSkillId, "id", "name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,type_skill_id,parent_skill_id")] skill skill)
        {
            if (ModelState.IsValid)
            {
                db.skills.Add(skill);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.type_skill_id = new SelectList(db.type_skill, "id", "name", skill.type_skill_id);

            var parentSkillId = db.skills.Where(s => s.parent_skill_id == null);
            ViewBag.parent_skill_id = new SelectList(parentSkillId, "id", "name", skill.parent_skill_id);

            return View(skill);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            skill skill = db.skills.Find(id);

            if (skill == null)
            {
                return HttpNotFound();
            }

            ViewBag.type_skill_id = new SelectList(db.type_skill, "id", "name", skill.type_skill_id);

            var parentSkillId = db.skills.Where(s => s.parent_skill_id == null);
            ViewBag.parent_skill_id = new SelectList(parentSkillId, "id", "name", skill.parent_skill_id);

            return View(skill);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,type_skill_id,parent_skill_id")] skill skill)
        {
            if (ModelState.IsValid)
            {
                db.Entry(skill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.type_skill_id = new SelectList(db.type_skill, "id", "name", skill.type_skill_id);

            var parentSkillId = db.skills.Where(s => s.parent_skill_id == null);
            ViewBag.parent_skill_id = new SelectList(parentSkillId, "id", "name", skill.parent_skill_id);

            return View(skill);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            skill skill = db.skills.Find(id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            return View(skill);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            skill skill = db.skills.Find(id);
            db.skills.Remove(skill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
