﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Chart;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class ChartController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Chart/
        public ActionResult Index()
        {
            return View();
        }

        //Biểu đồ số lượng dự án của từng nhóm
        public ActionResult TeamProjectCount()
        {
            var chartDraw = new ChartDraw();

            var q = from t in _db.teams
                    select new { t.name, value = t.projects.Count() };

            //chartDraw.ProjectCount = new List<ChartValue>();
            foreach (var item in q)
            {
                chartDraw.ProjectCount.Add(new ChartValue
                {
                    name = item.name,
                    value = item.value
                });
            }

            return View(chartDraw);
        }

        //Biểu đồ số lượng dự án theo quy mô
        public ActionResult ScaleProjects()
        {
            var chartDraw = new ChartDraw();

            var q = (from p in _db.projects
                     select new { QM = p.scale_estimator }).ToList();

            var q1 = q.Count(s => s.QM <= 3);
            var q2 = q.Count(s => s.QM > 3 && s.QM <= 10);
            var q3 = q.Count(s => s.QM > 10 && s.QM <= 50);
            var q4 = q.Count(s => s.QM > 50);

            chartDraw.RevenueTeam = new List<ChartValue>
            {
                new ChartValue { name = "< 3 man/month", value = q1 },
                new ChartValue { name = "3 - 10 man/month", value = q2 },
                new ChartValue { name = "10 - 50 man/month", value = q3 },
                new ChartValue { name = "> 50 man/month", value = q4 }
            };

            return View(chartDraw);
        }

        //Biểu đồ phần trăm doanh thu giữa các team
        public ActionResult RevenueTeams()
        {
            var chartDraw = new ChartDraw();

            //Phần trăm doanh thu giữa các team
            var q = from t in _db.teams
                    select new
                    {
                        t.name,
                        value = t.projects.Where(p => p.end_project != null).Sum(p => p.contract_value)
                    };

            //chartDraw.RevenueTeam = new List<ChartValue>();
            foreach (var item in q)
            {
                chartDraw.RevenueTeam.Add(new ChartValue
                {
                    name = item.name,
                    value = item.value.GetValueOrDefault()
                });
            }

            //Phần trăm lợi nhuận giữa các team
            var q1 = from t in _db.teams
                     select new
                     {
                         t.name,
                         value = t.projects.Where(p => p.end_project != null).Sum(p => p.contract_value - p.current_cost)
                     };

            //chartDraw.ProfitTeams = new List<ChartValue>();
            foreach (var item in q1)
            {
                chartDraw.ProfitTeams.Add(new ChartValue
                {
                    name = item.name,
                    value = item.value.GetValueOrDefault()
                });
            }

            //Hiệu suất doanh thu/người từng team
            var q2 = from t in _db.teams
                     select new
                     {
                         t.name,
                         value = t.projects.Where(p => p.end_project != null).Sum(p => p.contract_value) / t.team_staff.Count()
                     };

            //chartDraw.RevenuePersonalTeam = new List<ChartValue>();
            foreach (var item in q2)
            {
                chartDraw.RevenuePersonalTeam.Add(new ChartValue
                {
                    name = item.name,
                    value = item.value.GetValueOrDefault()
                });
            }

            return View(chartDraw);
        }

        public ActionResult IsFailedTeam()
        {
            var chartDraw = new List<ChartDraw>();

            //Biểu đồ thành công/thất bại toàn công ty
            var q1 = from p in _db.end_project
                     where p.is_fail == true
                     select p;

            var q2 = from p in _db.end_project
                     select p;

            chartDraw.Add(new ChartDraw
            {
                name = "Toàn công ty",
                FailedEachTeam = new List<ChartValue>
                {
                        new ChartValue { name = "Thành công", value = q2.Count() - q1.Count() },
                        new ChartValue { name = "Thất bại", value = q1.Count() }
                    }
            });

            //Biểu đồ tỷ lệ thành công/thất bại của từng nhóm
            var q = (from t in _db.teams
                     where t.projects.Count() != 0 && (t.projects.Count(s => s.end_project.is_fail == true) != 0
                           || t.projects.Count(s => s.end_project.is_fail == false) != 0)
                     select new
                     {
                         t.name,
                         failed = t.projects.Count(s => s.end_project.is_fail == true),
                         sucess = t.projects.Count(s => s.end_project.is_fail == false)
                     }).ToList();

            chartDraw.AddRange(q.Select(item => new ChartDraw
            {
                name = item.name,
                FailedEachTeam = new List<ChartValue>
                {
                    new ChartValue {name = "Thành công", value = item.sucess},
                    new ChartValue {name = "Thất bại", value = item.failed}
                }
            }));

            return View(chartDraw);
        }

        //Biểu đồ số lượng dự án, doanh thu dự án theo năm
        public ActionResult RevenueYears()
        {
            var chartDraw = new ChartDraw();

            //Số lượng dự án theo năm
            var q = from p in _db.projects
                    group p by p.expected_start_date.Year into g
                    orderby g.Key
                    select g;

            foreach (var item in q)
            {
                chartDraw.NumberYear.Add(new ChartValue
                {
                    name = item.Key.ToString(CultureInfo.InvariantCulture),
                    value = item.Count()
                });
            }

            //Doanh thu dự án theo năm
            var q1 = from p in _db.projects
                     where p.end_project != null
                     group p by p.expected_start_date.Year into g
                     orderby g.Key
                     select g;

            foreach (var item in q1)
            {
                chartDraw.RevenueYear.Add(new ChartValue
                {
                    name = item.Key.ToString(CultureInfo.InvariantCulture),
                    value = item.Sum(p => p.contract_value).GetValueOrDefault()
                });
            }

            return View(chartDraw);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}