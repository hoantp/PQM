﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class TeamController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Team/
        public ActionResult Index()
        {
            var q = _db.teams.OrderBy(x => x.display_order).Include(s => s.team_staff).ToList();
            foreach (var team in q)
            {
                GetStaffInTeam(team);
            }

            return View(_db.teams.OrderBy(x => x.display_order).ToList());
        }

        // GET: /Team/Details/5
        public ActionResult Details(int? id, string startdate, string enddate)
        {
            startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
            enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;
            IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
            var sd = DateTime.Parse(startdate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            var ed = DateTime.Parse(enddate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            var d = from w in _db.works
                    where w.work_day >= sd && w.work_day <= ed
                    group w by w.work_day into g
                    select g.Key;
            ViewBag.day = d.OrderBy(x => x.Value).ToList();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var team = _db.teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
           /* var day = from ts in team.team_staff
                      from r in ts.staff.resources
                      from w in r.works
                      group w by w.work_day into g
                      select g.Key;
            ViewBag.day = day.OrderBy(x => x.Value).ToList();*/



            return View(team);
        }

        // GET: /Team/Create
        public ActionResult Create()
        {
            var team = new team { team_staff = new List<team_staff>() };
            PopulateAssignedStaffData(team);
            return View();
        }

        // POST: /Team/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,workplace,technique,display_order")] team team, string[] to_select_list, string[] leader_select_list)
        {
            if (to_select_list != null || leader_select_list != null)
            {
                team.team_staff = new List<team_staff>();

                if (to_select_list != null)
                {
                    foreach (var staff in to_select_list)
                    {
                        var staffToAdd = new team_staff { id_staff = int.Parse(staff), leader = false, is_main_team = true };
                        team.team_staff.Add(staffToAdd);
                    }
                }

                if (leader_select_list != null)
                {
                    foreach (var staff in leader_select_list)
                    {
                        var leaderToAdd = new team_staff { id_staff = int.Parse(staff), leader = true, is_main_team = true };
                        team.team_staff.Add(leaderToAdd);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                _db.teams.Add(team);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(team);
        }

        // GET: /Team/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //team team = db.teams.Find(id);

            team team = _db.teams.Include(i => i.team_staff).Single(i => i.id == id);
            PopulateAssignedStaffData(team);

            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: /Team/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string[] to_select_list, string[] leader_select_list)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var teamToUpdate = _db.teams.Include(i => i.team_staff).Single(i => i.id == id);

            if (TryUpdateModel(teamToUpdate, ""))
            {
                try
                {
                    UpdateTeamStaffs(to_select_list, leader_select_list, teamToUpdate);

                    _db.Entry(teamToUpdate).State = EntityState.Modified;
                    _db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            PopulateAssignedStaffData(teamToUpdate);
            return View(teamToUpdate);
        }

        // GET: /Team/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            team team = _db.teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: /Team/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            team team = _db.teams.Find(id);

            var q = _db.team_staff;
            foreach (var staff in q.Where(staff => staff.id_team == team.id))
            {
                _db.team_staff.Remove(staff);
            }

            var allProjects = new HashSet<int>(team.projects.Select(p => p.id));
            foreach (var project in _db.projects)
            {
                if (allProjects.Contains(project.id))
                {
                    team.projects.Remove(project);
                }
            }

            _db.teams.Remove(team);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        // Lấy danh sách nhân sự đã có trong team
        private void GetStaffInTeam(team team)
        {
            var allStaffs = _db.staffs;
            var teamStaffs = new HashSet<int>(team.team_staff.Where(c => c.leader == false).Select(c => c.id_staff));
            var teamLeaders = new HashSet<int>(team.team_staff.Where(c => c.leader == true).Select(c => c.id_staff));

            var viewModel = new List<AssignedStaffData>();
            var viewLeader = new List<AssignedLeaderData>();

            foreach (var staff in allStaffs)
            {
                if (teamStaffs.Contains(staff.id))
                {
                    viewModel.Add(new AssignedStaffData
                    {
                        StaffId = staff.id,
                        Name = staff.name
                    });
                }

                if (teamLeaders.Contains(staff.id))
                {
                    viewLeader.Add(new AssignedLeaderData
                    {
                        LeaderId = staff.id,
                        Name = staff.name
                    });
                }
            }
            team.getStaffs = viewModel;
            team.getLeaders = viewLeader;
        }

        // Lấy danh sách nhân sự (Thông qua View Model A.S.D)
        private void PopulateAssignedStaffData(team team)
        {
            var allStaffs = _db.staffs;

            var staffsInTeam = new HashSet<int>(team.team_staff.Select(c => c.id_staff));
            var teamStaffs = new HashSet<int>(team.team_staff.Where(c => c.leader != true).Select(c => c.id_staff));
            var leaderTeams = new HashSet<int>(team.team_staff.Where(l => l.leader == true).Select(l => l.id_staff));

            var viewModel = new List<AssignedStaffData>();
            var viewLeader = new List<AssignedLeaderData>();
            var viewSit = new List<AssignedSit>();

            foreach (var staff in allStaffs)
            {
                viewSit.Add(new AssignedSit
                {
                    StaffId = staff.id,
                    Name = staff.name,
                    Assigned = staffsInTeam.Contains(staff.id)
                });

                viewModel.Add(new AssignedStaffData
                {
                    StaffId = staff.id,
                    Name = staff.name,
                    Assigned = teamStaffs.Contains(staff.id)
                });

                viewLeader.Add(new AssignedLeaderData
                {
                    LeaderId = staff.id,
                    Name = staff.name,
                    Assigned = leaderTeams.Contains(staff.id)
                });
            }

            ViewBag.Staffs = viewModel;
            ViewBag.Leaders = viewLeader;
            ViewBag.SIT = _db.staffs.Where(s => s.team_staff.Any() != true).ToList();
        }

        // Update dữ liệu trong bảng team_staff
        private void UpdateTeamStaffs(IEnumerable<string> selectedStaffs, string[] selectedLeader, team teamToUpdate)
        {
            if (selectedStaffs == null && selectedLeader == null)
            {
                teamToUpdate.team_staff = new List<team_staff>();
                return;
            }

            if (selectedStaffs != null)
            {
                var selectedStaffsHs = new HashSet<string>(selectedStaffs);

                var teamStaffs = new HashSet<int>(teamToUpdate.team_staff.Select(c => c.id_staff));
                var q = _db.team_staff.Where(s => s.id_team == teamToUpdate.id).ToList();

                foreach (var staff in _db.staffs)
                {
                    if (selectedStaffsHs.Contains(staff.id.ToString(CultureInfo.InvariantCulture)))
                    {
                        if (!teamStaffs.Contains(staff.id))
                        {
                            var staffToExe = new team_staff { id_staff = staff.id, leader = false, is_main_team = true };
                            teamToUpdate.team_staff.Add(staffToExe);
                        }
                        else
                        {
                            foreach (var item in q)
                            {
                                item.leader = false;
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in q)
                        {
                            if (item.id_staff == staff.id && teamStaffs.Contains(staff.id))
                            {
                                teamToUpdate.team_staff.Remove(item);
                            }
                        }
                    }

                    if (selectedLeader != null)
                    {
                        var selectedLeaderHs = new HashSet<string>(selectedLeader);
                        if (!selectedLeaderHs.Contains(staff.id.ToString(CultureInfo.InvariantCulture))) continue;
                        var staffToExe = new team_staff { id_staff = staff.id, is_main_team = true, leader = true };
                        teamToUpdate.team_staff.Add(staffToExe);
                    }
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
