﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class StaffController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Staff/
        public ActionResult Index()
        {
            return View(_db.staffs.Where(s => s.is_deleted == false).ToList());
        }

        // GET: /Staff/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var staff = _db.staffs.Find(id);
            if (staff == null)
            {
                return HttpNotFound();
            }

            return View(staff);
        }

        // GET: /Staff/Create
        public ActionResult Create()
        {
            ViewBag.team_id = new SelectList(_db.teams, "id", "name");
            ViewBag.position_id = new SelectList(_db.positions, "id", "name");
             var t = _db.type_skill.Include(x => x.skills).ToList();
            ViewBag.type_skill = t;
            return View();
        }

        // POST: /Staff/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,email,skype,phone,position_id,display_order")] staff staff, int? team_id)
        {

            if (team_id.HasValue)
            {
                staff.team_staff.Add(new team_staff { id_team = team_id.Value, is_main_team = true });
            }
            if (ModelState.IsValid)
            {
                _db.staffs.Add(staff);
                _db.SaveChanges();

                return RedirectToAction("AddSkill/"+ staff.id);
            }
            
            return View(staff);
        }

        // GET: /Staff/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var mainTeam = from ts in _db.team_staff
                              where ts.id_staff == id.Value && ts.is_main_team == true
                              select ts.id_team;
            var mt = mainTeam.ToList();
            ViewBag.team_id = mt.Count > 0 ? new SelectList(_db.teams, "id", "name", mt.First()) : new SelectList(_db.teams, "id", "name");
            staff s = _db.staffs.Find(id);

            ViewBag.position_id = new SelectList(_db.positions, "id", "name", s.position_id);
            var staff = _db.staffs.Find(id);
            if (staff == null)
            {
                return HttpNotFound();
            }
            return View(staff);
        }

        // POST: /Staff/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,email,skype,phone,position_id,display_order")] staff staff, int? team_id)
        {
            var ts = _db.team_staff.Where(s => s.id_staff == staff.id && s.is_main_team == true).ToList();
            var mainTeam = ts.First();
            if (ts.Count > 0)
            {
                if (team_id.HasValue)
                {
                    mainTeam.id_team = team_id.Value;

                }
                else
                {
                    mainTeam.is_main_team = false;
                }
            }
            else
            {
                if (team_id.HasValue)
                    staff.team_staff.Add(new team_staff { id_team = team_id.Value, is_main_team = true });
            }

            if (ModelState.IsValid)
            {
                _db.Entry(staff).State = EntityState.Modified;
                //db.Entry(_staff).CurrentValues.SetValues(staff);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(staff);
        }

        // GET: /Staff/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var staff = _db.staffs.Find(id);
            if (staff == null)
            {
                return HttpNotFound();
            }
            return View(staff);
        }

        // POST: /Staff/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            staff staff = _db.staffs.Find(id);
            //foreach (var item in _db.team_staff)
            //{
            //    if (item.id_staff == id)
            //    {
            //        _db.team_staff.Remove(item);
            //    }
            //}

            //foreach (var r in staff.resources)
            //{
            //    foreach (var w in r.works)
            //    {
            //        _db.works.Remove(w);
            //    }
            //    _db.resources.Remove(r);
            //}
            //foreach (var s in _db.staff_of_skill)
            //{
            //    if (s.staff_id == id)
            //    {
            //        _db.staff_of_skill.Remove(s);
            //    }
            //}
            try
            {
                //_db.staffs.Remove(staff);
                staff.is_deleted = true;
                _db.Entry(staff).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Không xóa được vì nhân sự này đang trong dự án");
            }
            return View(staff);
        }


        public ActionResult AddSkill(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var staffOfSkill = _db.staff_of_skill.Where(s => s.staff_id == id).ToList();

            ViewBag.ParentSkills = _db.skills.Where(s => s.parent_skill_id == null).ToList();
            ViewBag.ChildSkills = _db.skills.Where(s => s.parent_skill_id != null).ToList();
            ViewBag.SkillLevels = _db.levels.ToList();

            return View(staffOfSkill);
        }

        [HttpPost]
        public ActionResult AddSkill(int id, string[] skills, string[] levels, string[] num_exp)
        {
            var oldData = _db.staff_of_skill.Where(s => s.staff_id == id);
            if (oldData.Any())
            {
                _db.staff_of_skill.RemoveRange(oldData);
                _db.SaveChanges();
            }

            for (int i = 0; i < skills.Count(); i++)
            {
                var staffSkill = new staff_of_skill
                {
                    staff_id = id,
                    skill_id = Convert.ToInt32(skills[i]),
                    level_id = Convert.ToInt32(levels[i]),
                    num_exp = Convert.ToInt32(num_exp[i])
                };

                _db.staff_of_skill.Add(staffSkill);
            }

            _db.SaveChanges();

            return View("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
