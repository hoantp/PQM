﻿using System.Linq;
using System.Web.Mvc;
using QLCLDA.Models;
using System.Web.Security;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class AccountController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Account/
        public ActionResult Index()
        {
            
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        // GET: /Account/Login
        public ActionResult Login(string returnUrl)
        {
            //_db.Database.Connection.ConnectionString = "";
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Account user, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (user.IsValid(user.UserName, user.Password))
                {
                    FormsAuthentication.SetAuthCookie(user.UserName, user.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Sai tên đăng nhập hoặc mật khẩu!");
            }
            return View(user);
        }

        // POST: Log out
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }


        // GET: /Account/ChangePassword
        public ActionResult ChangePassword()
        {
            var muser = new ManagerUsers();
            return View(muser);
        }

        // POST: /Account/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ManagerUsers muser)
        {
            if (ModelState.IsValid)
            {
                if (muser.IsValid(User.Identity.Name, muser.OldPassword))
                {
                    var userdata = _db.users.First(i => i.email == User.Identity.Name);
                    userdata.password = muser.NewPassword;
                    _db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
                
                ModelState.AddModelError("", "Mật khẩu cũ không khớp!");
            }
            return View(muser);
        }

        //// GET:
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST:
        //[HttpPost]
        //public ActionResult Create([Bind(Include = "id,email,password")] user user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.users.Add(user);
        //        db.SaveChanges();
        //        return RedirectToAction("Login");
        //    }

        //    return View(user);
        //}
    }
}