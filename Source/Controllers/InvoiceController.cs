﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using OfficeOpenXml;
using QLCLDA.ActionSelector;
using QLCLDA.Models;
using QLCLDA.Models.Entity;
using Excel = Microsoft.Office.Interop.Excel;

namespace QLCLDA.Controllers
{
    public class InvoiceController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Invoice/
        public ActionResult Index()
        {
            ViewBag.idCustomer = new SelectList(_db.customers, "id", "company");
            var projects = _db.projects.Where(p => p.end_project != null);
            ViewBag.Projects = projects.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "ExportToExcel")]
        public ActionResult ExportToExcel(int idCustomer, string tax, string[] to_select_list)
        {
            var newFile = ConvertToExcel(idCustomer, tax, to_select_list);

            // Open Dialog Save File
            Response.AddHeader("Content-disposition", "attachment; filename=output.xlsx");
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(newFile);
            Response.Flush();
            System.IO.File.Delete(newFile);
            Response.End();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "ExportToPdf")]
        public ActionResult ExportToPdf(int idCustomer, string tax, string[] to_select_list)
        {
            var appData = Server.MapPath("~/App_Data");
            var newPdf = string.Format("{0}.pdf", Guid.NewGuid());
            var pdfFile = Path.Combine(appData, newPdf);

            var newFile = ConvertToExcel(idCustomer, tax, to_select_list);

            Excel._Application excelApplication = new Excel.Application();

            var paramSourceBookPath = newFile;
            var paramMissing = Type.Missing;

            var paramExportFilePath = pdfFile;
            const Excel.XlFixedFormatType paramExportFormat = Excel.XlFixedFormatType.xlTypePDF;
            const Excel.XlFixedFormatQuality paramExportQuality = Excel.XlFixedFormatQuality.xlQualityStandard;
            const bool paramOpenAfterPublish = false;
            const bool paramIncludeDocProps = true;
            const bool paramIgnorePrintAreas = true;
            var paramFromPage = Type.Missing;
            var paramToPage = Type.Missing;

            // Open the source workbook.
            var excelWorkBook = excelApplication.Workbooks.Open(paramSourceBookPath,
                                                                paramMissing, paramMissing, paramMissing, paramMissing,
                                                                paramMissing, paramMissing, paramMissing, paramMissing,
                                                                paramMissing, paramMissing, paramMissing, paramMissing,
                                                                paramMissing, paramMissing);

            // Save it in the target format.
            if (excelWorkBook != null)
                excelWorkBook.ExportAsFixedFormat(paramExportFormat,
                                                  paramExportFilePath, paramExportQuality,
                                                  paramIncludeDocProps, paramIgnorePrintAreas, paramFromPage,
                                                  paramToPage, paramOpenAfterPublish,
                                                  paramMissing);

            // Close the workbook object.
            if (excelWorkBook != null)
            {
                excelWorkBook.Close(false, paramMissing, paramMissing);
            }

            // Quit Excel and release the ApplicationClass object.
            excelApplication.Quit();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            // Open Dialog Save File
            Response.AddHeader("Content-disposition", "attachment; filename=output.pdf");
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(paramExportFilePath);
            Response.Flush();
            System.IO.File.Delete(paramSourceBookPath);
            System.IO.File.Delete(paramExportFilePath);
            Response.End();

            return RedirectToAction("Index");
        }

        private string ConvertToExcel(int idCustomer, string tax, IEnumerable<string> selectedProjects)
        {
            var appData = Server.MapPath("~/App_Data");
            const string templateFile = "Test3.xlsx";
            var newFileName = string.Format("{0}.xlsx", Guid.NewGuid());
            var originFile = Path.Combine(appData, templateFile);
            var newFile = Path.Combine(appData, newFileName);
            var time = DateTime.Now;
            const string dateInvoice = "　yyyy　年　　M　月　d　日　";
            const string noInvoice = "　No.     RSVN-JP-yyyyMM-1";
            const string sheetInvoice = "請求書_yyyyMM";
            var i = 17;

            using (var templateXls = new ExcelPackage(new FileInfo(originFile)))
            {
                var template = templateXls.Workbook.Worksheets[1];
                var sheet = templateXls.Workbook.Worksheets.Add(time.ToString(sheetInvoice), template);
                sheet.Cells["H1"].Value = time.ToString(noInvoice);
                sheet.Cells["H3"].Value = time.ToString(dateInvoice);
                sheet.Cells["H15"].Value = int.Parse(tax);
                sheet.Cells["B7"].Value = _db.customers.Single(c => c.id == idCustomer).company;
                sheet.Cells["B8"].Value = _db.customers.Single(c => c.id == idCustomer).address;
                sheet.Cells["B9"].Value = "TEL: " + _db.customers.Single(c => c.id == idCustomer).phone +
                                          "/ FAX: " + _db.customers.Single(c => c.id == idCustomer).fax;
                foreach (var id in selectedProjects.Select(int.Parse))
                {
                    sheet.Cells[i, 2].Value = _db.projects.Single(p => p.id == id).name_project;
                    sheet.Cells[i, 6].Value = 1;
                    sheet.Cells[i, 7].Value = _db.projects.Single(p => p.id == id).contract_value;
                    i++;
                }
                templateXls.SaveAs(new FileInfo(newFile));
            }

            return newFile;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
