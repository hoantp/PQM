﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml;
using System.IO;
using System.Globalization;
using QLCLDA.Models.Entity;
using QLCLDA.Models.ExtModel;
using EntityState = System.Data.Entity.EntityState;

namespace QLCLDA.Controllers
{
    public class ResourceController : Controller
    {
        private qlcldaEntities db = new qlcldaEntities();

        // GET: /Resource/
        public ActionResult Index(int? team_id, string startdate, string enddate, string choose)
        {
            ViewBag.startdate = startdate;
            ViewBag.enddate = enddate;
            ViewBag.choose = choose;

            startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
            enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;
            IFormatProvider culture = new CultureInfo("fr-FR", true);
            var sd = DateTime.Parse(startdate, culture, DateTimeStyles.AssumeLocal);
            var ed = DateTime.Parse(enddate, culture, DateTimeStyles.AssumeLocal);
            var d = db.works.Where(w => w.work_day >= sd && w.work_day <= ed)
                            .GroupBy(w => w.work_day)
                            .Select(g => g.Key);
            ViewBag.day = d.OrderBy(x => x.Value).ToList();

            ViewBag.team_id = new SelectList(db.teams, "id", "name");
            if (team_id == null)
            {


                /*var staff = from s in db.staffs
                            from w in db.works
                            from r in db.resources
                            where s.id == r.staff_id && w.resource_id == r.id
                            && w.work_day >= sd && w.work_day <= ed
                            select s;*/

                return View(db.teams.Distinct().ToList());
            }
            /*                
                            var staff = from s in db.staffs
                                        from ts in db.team_staff
                                        from w in db.works
                                        from r in db.resources
                                        where s.id == ts.id_staff && ts.id_team == team_id && s.id == r.staff_id && w.resource_id == r.id
                                        && w.work_day >= sd && w.work_day <= ed
                                        select s;*/

            var teams = db.teams.Where(t => t.id == team_id);

            return View(teams.ToList());
        }


        // GET: /Resource/Create
        public ActionResult Create()
        {
            ViewBag.project_id = new SelectList(db.projects, "id", "name_cd");
            ViewBag.staff_id = new SelectList(db.staffs, "id", "name");
            return View();
        }

        // POST: /Resource/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,staff_id,project_id")] resource resource, string startdate, string enddate, int percent, int? isweekend)
        {
            var x = from r in db.resources
                    where r.project_id == resource.project_id && r.staff_id == resource.staff_id
                    select r.id;



            if (ModelState.IsValid)
            {
                if (!x.Any())
                {
                    db.resources.Add(resource);
                    db.SaveChanges();

                    var res = from r in db.resources
                              where r.project_id == resource.project_id && r.staff_id == resource.staff_id
                              select r;
                    var dates = new List<DateTime>();
                    startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
                    enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;
                    IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
                    var start = DateTime.Parse(startdate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                    var end = DateTime.Parse(enddate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                    for (var dt = start; dt <= end; dt = dt.AddDays(1))
                    {
                        if (isweekend != null || (dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday))
                        {
                            var w = new work
                            {
                                percent = percent,
                                resource_id = res.First().id,
                                work_day = dt
                            };
                            db.works.Add(w);
                        }
                    }
                }
                else
                {
                    var dates = new List<DateTime>();
                    startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
                    enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;
                    IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
                    var start = DateTime.Parse(startdate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                    var end = DateTime.Parse(enddate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                    for (var dt = start; dt <= end; dt = dt.AddDays(1))
                    {
                        if (isweekend != null || (dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday))
                        {
                            var i = 0;
                            var dt1 = dt;
                            foreach (var wk in db.works.ToList().Where(wk => wk.work_day != null && DateTime.Compare(wk.work_day.Value, dt1) == 0 && wk.resource_id == x.First()))
                            {
                                wk.percent = percent;
                                db.Entry(wk).State = EntityState.Modified;
                                i = 1;
                                break;
                            }
                            if (i != 0) continue;
                            var w = new work
                            {
                                percent = percent,
                                resource_id = x.First(),
                                work_day = dt
                            };
                            db.works.Add(w);
                        }
                    }
                }

                db.SaveChanges();
            }

            ViewBag.project_id = new SelectList(db.projects, "id", "name_cd", resource.project_id);
            ViewBag.staff_id = new SelectList(db.staffs, "id", "name", resource.staff_id);
            return View(resource);
        }

        [HttpPost]
        public ActionResult Add(int project_id, int staff_id, string startdate, string enddate, int percent, int? isweekend)
        {
            var x = db.resources.Where(r => r.project_id == project_id && r.staff_id == staff_id).Select(r => r.id);

            var resource = new resource
            {
                project_id = project_id,
                staff_id = staff_id
            };

            //Xóa dữ liệu cũ
            var oldData = db.resources.FirstOrDefault(r => r.project_id == project_id && r.staff_id == staff_id);
            if (null != oldData)
            {
                var models = db.works.Where(w => w.resource_id == oldData.id).ToList();
                db.works.RemoveRange(models);
                db.SaveChanges();
                db.resources.Remove(oldData);
                db.SaveChanges();
            }

            if (ModelState.IsValid)
            {
                if (!x.Any())
                {
                    db.resources.Add(resource);
                    db.SaveChanges();

                    var res = db.resources.Where(r => r.project_id == resource.project_id && r.staff_id == resource.staff_id);
                    var dates = new List<DateTime>();

                    startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
                    enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;

                    IFormatProvider culture = new CultureInfo("fr-FR", true);
                    var start = DateTime.Parse(startdate, culture, DateTimeStyles.AssumeLocal);
                    var end = DateTime.Parse(enddate, culture, DateTimeStyles.AssumeLocal);
                    for (var dt = start; dt <= end; dt = dt.AddDays(1))
                    {
                        if (isweekend != null || (dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday))
                        {
                            var w = new work
                            {
                                percent = percent,
                                resource_id = res.First().id,
                                work_day = dt
                            };
                            db.works.Add(w);
                        }
                    }
                }
                else
                {
                    var dates = new List<DateTime>();
                    startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
                    enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;
                    IFormatProvider culture = new CultureInfo("fr-FR", true);
                    var start = DateTime.Parse(startdate, culture, DateTimeStyles.AssumeLocal);
                    var end = DateTime.Parse(enddate, culture, DateTimeStyles.AssumeLocal);
                    for (var dt = start; dt <= end; dt = dt.AddDays(1))
                    {
                        if (isweekend != null || (dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday))
                        {
                            var i = 0;
                            var dt1 = dt;
                            foreach (var wk in db.works.ToList().Where(wk => wk.work_day != null && DateTime.Compare(wk.work_day.Value, dt1) == 0 && wk.resource_id == x.First()))
                            {
                                wk.percent = percent;
                                db.Entry(wk).State = EntityState.Modified;
                                i = 1;
                                break;
                            }
                            if (i != 0) continue;
                            var w = new work
                            {
                                percent = percent,
                                resource_id = x.First(),
                                work_day = dt
                            };
                            db.works.Add(w);
                        }
                    }
                }

                db.SaveChanges();
            }

            return Json("Đã thêm thành công danh sách công việc!", JsonRequestBehavior.AllowGet);
        }

        // GET: /Resource/Edit/5
        public ActionResult Edit(int? id, string startdate, string enddate)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            staff staff = db.staffs.Find(id);
            if (staff == null)
            {
                return HttpNotFound();
            }
            ViewBag.startdate = startdate;
            ViewBag.enddate = enddate;
            startdate = startdate == null || startdate.Length == 0 ? "01/01/2000" : startdate;
            enddate = enddate == null || enddate.Length == 0 ? "01/01/2222" : enddate;
            IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
            DateTime sd = DateTime.Parse(startdate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            DateTime ed = DateTime.Parse(enddate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            var d = from w in db.works
                    join r in db.resources on w.resource_id equals r.id
                    where r.staff_id == id //&& w.work_day >= sd && w.work_day <= ed
                    group w by w.work_day into g
                    select g.Key;
            ViewBag.day = d.ToList();

            ViewBag.team = new SelectList(db.teams, "id", "name");

            return View(staff);
        }


        public ActionResult Export(string startdate, string enddate, string choose)
        {
            string newFile = "";
            if (choose == "week")
            {

            }
            else
            {
                newFile = ConvertToExcel(startdate, enddate);
            }
            ConvertToExcel(startdate, enddate);

            Response.AddHeader("Content-disposition", "attachment; filename=Resource.xlsx");
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(newFile);
            Response.Flush();
            System.IO.File.Delete(newFile);
            Response.End();

            return RedirectToAction("Index");
        }

        private string ConvertToExcel(string startdate, string enddate)
        {
            startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
            enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;
            IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
            DateTime sd = DateTime.Parse(startdate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            DateTime ed = DateTime.Parse(enddate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            var d = from w in db.works
                    join r in db.resources on w.resource_id equals r.id
                    where w.work_day >= sd && w.work_day <= ed
                    group w by w.work_day into g
                    select g.Key;
            var days = d.ToList();


            var appData = Server.MapPath("~/App_Data");
            const string templateFile = "form_resource.xlsx";
            const string newFileName = "Resource.xlsx";
            var originFile = Path.Combine(appData, templateFile);
            var newFile = Path.Combine(appData, newFileName);

            using (var templateXls = new ExcelPackage(new FileInfo(originFile)))
            {
                var template = templateXls.Workbook.Worksheets[1];
                var sheet = template;

                var team = db.teams.ToList();
                int[] row = { 8 };
                int col = 5;
                foreach (var day in days)
                {
                    sheet.Cells[7, col].Value = day.Value.ToString("dd/MM/yyy");
                    col++;
                }

                if (team.Count > 0)
                {
                    foreach (var t in from t in team let a = row[0] select t)
                    {
                        sheet.Cells[row[0], 2].Value = t.name;
                        if (t.team_staff.Count > 0)
                        {

                            foreach (var ts in from ts in t.team_staff let b = row[0] select ts)
                            {
                                sheet.Cells[row[0], 3].Value = ts.staff.name;

                                if (ts.staff.resources.Count > 0)
                                {
                                    foreach (var r in ts.staff.resources)
                                    {
                                        col = 5;
                                        sheet.Cells[row[0], 4].Value = r.project.name_cd;
                                        foreach (var day in days)
                                        {
                                            var day1 = day;
                                            foreach (var w in r.works.Where(w => w.work_day != null && DateTime.Compare(day1.Value, w.work_day.Value) == 0))
                                            {
                                                sheet.Cells[row[0], col].Value = w.percent + "%";
                                                break;
                                            }
                                            col++;
                                        }
                                        row[0]++;
                                    }
                                    sheet.Cells[row[0], 3].Value = "Tổng phần trăm công việc";
                                    col = 5;
                                    foreach (var day in days)
                                    {
                                        var sumStaff = 0;

                                        foreach (var r in ts.staff.resources)
                                        {
                                            var day1 = day;
                                            foreach (var w in r.works.Where(w => w.work_day != null && DateTime.Compare(day1.Value, w.work_day.Value) == 0))
                                            {
                                                sumStaff += w.percent.Value;
                                                break;
                                            }
                                        }
                                        sheet.Cells[row[0], col].Value = sumStaff + "%";
                                        //sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(196, 189, 151)) ;
                                        col++;
                                    }
                                    row[0]++;
                                }
                                else
                                {
                                    row[0]++;
                                }
                            }
                            sheet.Cells[row[0], 2].Value = "Tổng số";
                            col = 5;
                            foreach (var day in days)
                            {
                                var sum_staff = 0;
                                foreach (var r in t.team_staff.SelectMany(ts => ts.staff.resources))
                                {
                                    foreach (var w in r.works)
                                    {
                                        if (DateTime.Compare(day.Value, w.work_day.Value) == 0)
                                        {
                                            sum_staff += w.percent.Value;
                                            break;
                                        }
                                    }
                                }
                                sheet.Cells[row[0], col].Value = sum_staff + "%/" + t.team_staff.Count + "00%";
                                sheet.Cells[row[0], col].Style.Fill = sheet.Cells[1, 1].Style.Fill;
                                col++;
                            }
                            row[0]++;
                        }
                        else
                        {
                            row[0]++;
                        }
                    }
                }

                templateXls.SaveAs(new FileInfo(newFile));
            }

            return newFile;
        }

        private string ConvertToExcelWeek(string startdate, string enddate)
        {
            startdate = string.IsNullOrEmpty(startdate) ? "01/01/2000" : startdate;
            enddate = string.IsNullOrEmpty(enddate) ? "01/01/2222" : enddate;
            IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
            DateTime sd = DateTime.Parse(startdate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            DateTime ed = DateTime.Parse(enddate, culture, System.Globalization.DateTimeStyles.AssumeLocal);
            var d = from w in db.works
                    join r in db.resources on w.resource_id equals r.id
                    where w.work_day >= sd && w.work_day <= ed
                    group w by w.work_day into g
                    select g.Key;
            var days = d.ToList();


            var appData = Server.MapPath("~/App_Data");
            const string templateFile = "form_resource.xlsx";
            var newFileName = "Resource.xlsx";
            var originFile = Path.Combine(appData, templateFile);
            var newFile = Path.Combine(appData, newFileName);

            using (var templateXls = new ExcelPackage(new FileInfo(originFile)))
            {
                var template = templateXls.Workbook.Worksheets[1];
                var sheet = template;

                var team = db.teams.ToList();
                int row = 8;
                int col = 5;

                //Điền tiêu đề thời gian
                for (var i = 0; i < days.Count; )
                {
                    var nextweek = days.ElementAt(i).Value.AddDays(8 - (int)days.ElementAt(i).Value.DayOfWeek);
                    var j = i;
                    for (; i < days.Count && days.ElementAt(i) < nextweek; i++) { }
                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    Calendar cal = dfi.Calendar;
                    sheet.Cells[7, col].Value = "Tuần " + cal.GetWeekOfYear(days.ElementAt(j).Value, dfi.CalendarWeekRule, dfi.FirstDayOfWeek) + "/" + days.ElementAt(j).Value.Year;
                    col++;
                }

                if (team.Count > 0)
                {
                    foreach (var t in team)
                    {
                        int a = row;
                        sheet.Cells[row, 2].Value = t.name;
                        if (t.team_staff.Count > 0)
                        {

                            foreach (var ts in t.team_staff)
                            {
                                int b = row;
                                sheet.Cells[row, 3].Value = ts.staff.name;

                                if (ts.staff.resources.Count > 0)
                                {
                                    foreach (var r in ts.staff.resources)
                                    {
                                        col = 5;
                                        sheet.Cells[row, 4].Value = r.project.name_cd;


                                        for (int i = 0; i < days.Count; i++)
                                        {

                                        }


                                        //Điền phần trăm công việc
                                        foreach (var day in days)
                                        {

                                            // Điền các cột ngày
                                            foreach (var w in r.works)
                                            {
                                                if (DateTime.Compare(day.Value, w.work_day.Value) == 0)
                                                {
                                                    sheet.Cells[row, col].Value = w.percent + "%";
                                                    break;
                                                }
                                            }
                                            col++;
                                        }
                                        row++;
                                    }
                                    sheet.Cells[row, 3].Value = "Tổng phần trăm công việc";
                                    col = 5;


                                    //Điền tổng phần trăm công việc của nhân viên
                                    foreach (var day in days)
                                    {
                                        int sumStaff = 0;

                                        foreach (var r in ts.staff.resources)
                                        {
                                            foreach (var w in r.works)
                                            {
                                                if (DateTime.Compare(day.Value, w.work_day.Value) == 0)
                                                {
                                                    sumStaff += w.percent.Value;
                                                    break;
                                                }
                                            }
                                        }
                                        sheet.Cells[row, col].Value = sumStaff + "%";
                                        //sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(196, 189, 151)) ;
                                        col++;
                                    }
                                    row++;
                                }
                                else
                                {
                                    row++;
                                }
                                //sheet.Cells[b , 2, row, 2].Merge = true;
                            }
                            sheet.Cells[row, 2].Value = "Tổng số";
                            col = 5;
                            foreach (var day in days)
                            {
                                int sum_staff = 0;
                                foreach (var ts in t.team_staff)
                                {

                                    foreach (var r in ts.staff.resources)
                                    {
                                        foreach (var w in r.works)
                                        {
                                            if (DateTime.Compare(day.Value, w.work_day.Value) == 0)
                                            {
                                                sum_staff += w.percent.Value;
                                                break;
                                            }
                                        }
                                    }

                                }
                                sheet.Cells[row, col].Value = sum_staff + "%/" + t.team_staff.Count + "00%";
                                sheet.Cells[row, col].Style.Fill = sheet.Cells[1, 1].Style.Fill;
                                col++;
                            }
                            row++;
                        }
                        else
                        {
                            row++;
                        }
                    }
                }

                templateXls.SaveAs(new FileInfo(newFile));
            }

            return newFile;
        }

        public ActionResult AddResource()
        {
            ViewBag.project_id = new SelectList(db.projects, "id", "name_cd");
            ViewBag.staff_id = new SelectList(db.staffs, "id", "name");
            return View();
        }

        public ActionResult GetStaffOfProject(int projectId)
        {
            var list = from r in db.resources
                       join s in db.staffs on r.staff_id equals s.id
                       where r.project_id == projectId && r.staff.is_deleted == false
                       select r;

            var worker = new List<Worker>();
            foreach (var resource in list.ToList())
            {
                var staffInWork = db.works.Where(s => s.resource_id == resource.id).ToList();

                if (staffInWork.Count > 0)
                {
                    worker.Add(new Worker
                    {
                        Name = resource.staff.name,
                        StartDate = DateTime.UtcNow,
                        EndDate = staffInWork.Max(s => s.work_day).GetValueOrDefault(),
                        Percent = staffInWork.Select(s => s.percent).First()
                    });
                }
            }

            return View(worker);
        }

        public ActionResult GetProjectByStaff(int staffId)
        {
            var list = from r in db.resources
                       join s in db.staffs on r.staff_id equals s.id
                       where r.staff_id == staffId && r.staff.is_deleted == false
                       select r;

            var worker = new List<Worker>();
            foreach (var resource in list.ToList())
            {
                var projectInWork = db.works.Where(s => s.resource_id == resource.id).ToList();

                if (projectInWork.Count > 0)
                {
                    worker.Add(new Worker
                    {
                        Name = resource.project.name_cd,
                        StartDate = DateTime.UtcNow,
                        EndDate = projectInWork.Max(s => s.work_day).GetValueOrDefault(),
                        Percent = projectInWork.Select(s => s.percent).First()
                    });
                }
            }

            return View(worker);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult GetTeamData()
        {
            var db2 = new qlcldaEntities();
            var data = from t in db2.teams
                       select t;

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
