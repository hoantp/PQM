﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class ActivityController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Activity/
        public ActionResult Index()
        {
            var activityLog = _db.activity_log.Include(a => a.project);
            return View(activityLog.ToList());
        }

        // GET: /Activity/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            activity_log activityLog = _db.activity_log.Find(id);
            if (activityLog == null)
            {
                return HttpNotFound();
            }
            return View(activityLog);
        }

        // GET: /Activity/Create
        public ActionResult Create()
        {
            ViewBag.id_project = new SelectList(_db.projects, "id", "name_project");
            return View();
        }

        // POST: /Activity/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,id_project,who_activity,date_update,content_update")] activity_log activityLog)
        {
            if (ModelState.IsValid)
            {
                _db.activity_log.Add(activityLog);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_project = new SelectList(_db.projects, "id", "name_project", activityLog.id_project);
            return View(activityLog);
        }

        // GET: /Activity/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            activity_log activityLog = _db.activity_log.Find(id);
            if (activityLog == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_project = new SelectList(_db.projects, "id", "name_project", activityLog.id_project);
            return View(activityLog);
        }

        // POST: /Activity/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,id_project,who_activity,date_update,content_update")] activity_log activityLog)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(activityLog).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_project = new SelectList(_db.projects, "id", "name_project", activityLog.id_project);
            return View(activityLog);
        }

        // GET: /Activity/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            activity_log activityLog = _db.activity_log.Find(id);
            if (activityLog == null)
            {
                return HttpNotFound();
            }
            return View(activityLog);
        }

        // POST: /Activity/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            activity_log activityLog = _db.activity_log.Find(id);
            _db.activity_log.Remove(activityLog);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
