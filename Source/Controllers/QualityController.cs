﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class QualityController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Quality/
        public ActionResult Index()
        {
            var qualities = _db.qualities.Include(q => q.project);
            return View(qualities.ToList());
        }

        // GET: /Quality/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quality quality = _db.qualities.Find(id);
            if (quality == null)
            {
                return HttpNotFound();
            }
            return View(quality);
        }

        // GET: /Quality/Create
        public ActionResult Create()
        {
            ViewBag.project_id = new SelectList(_db.projects, "id", "name_project");
            return View();
        }

        // POST: /Quality/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="project_id,number_kloc,number_bug_of_team,number_bug_of_team_in_kloc,number_bug_of_customer,number_bug_of_customer_int_kloc,number_test_case,human_resource,time_complete")] quality quality)
        {
            if (ModelState.IsValid)
            {
                _db.qualities.Add(quality);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.project_id = new SelectList(_db.projects, "id", "name_project", quality.project_id);
            return View(quality);
        }

        // GET: /Quality/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quality quality = _db.qualities.Find(id);
            if (quality == null)
            {
                return HttpNotFound();
            }
            ViewBag.project_id = new SelectList(_db.projects, "id", "name_project", quality.project_id);
            return View(quality);
        }

        // POST: /Quality/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="project_id,number_kloc,number_bug_of_team,number_bug_of_team_in_kloc,number_bug_of_customer,number_bug_of_customer_int_kloc,number_test_case,human_resource,time_complete")] quality quality)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(quality).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.project_id = new SelectList(_db.projects, "id", "name_project", quality.project_id);
            return View(quality);
        }

        // GET: /Quality/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quality quality = _db.qualities.Find(id);
            if (quality == null)
            {
                return HttpNotFound();
            }
            return View(quality);
        }

        // POST: /Quality/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            quality quality = _db.qualities.Find(id);
            _db.qualities.Remove(quality);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
