﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QLCLDA.Models;
using QLCLDA.Models.Entity;
using EntityState = System.Data.Entity.EntityState;

namespace QLCLDA.Controllers
{
    public class SkillOfStaffController : Controller
    {
        private qlcldaEntities db = new qlcldaEntities();

        // GET: /SkillOfStaff/Create
        public ActionResult Create()
        {
            ViewBag.skill_id = new SelectList(db.skills, "id", "name");
            ViewBag.staff_id = new SelectList(db.staffs, "id", "name");
            return View();
        }

        // POST: /SkillOfStaff/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "staff_id,skill_id,description")] staff_of_skill staff_of_skill)
        {
            if (ModelState.IsValid)
            {
                db.staff_of_skill.Add(staff_of_skill);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.skill_id = new SelectList(db.skills, "id", "name", staff_of_skill.skill_id);
            ViewBag.staff_id = new SelectList(db.staffs, "id", "name", staff_of_skill.staff_id);
            return View(staff_of_skill);
        }


        public ActionResult Save(int staff_id, int skill_id, string description)
        {
            var s = db.staffs.Find(staff_id);
            var x = from sos in db.staff_of_skill
                    where sos.skill_id == skill_id && sos.staff_id == staff_id
                    select sos;
            var staffOfSkill = new staff_of_skill
            {
                skill_id = skill_id,
                staff_id = staff_id,
                description = description
            };

            if (ModelState.IsValid)
            {
                if (!x.Any())
                {
                    db.staff_of_skill.Add(staffOfSkill);
                }
                else
                {
                    db.Entry(staffOfSkill).State = EntityState.Modified;
                }
            }
            db.SaveChanges();
            return Json("Đã lưu thành kỹ năng!", JsonRequestBehavior.AllowGet);
        }


        // POST: /SkillOfStaff/Delete/
        public ActionResult Delete(int staff_id, int skill_id)
        {
            var x = from sos in db.staff_of_skill
                    where sos.skill_id == skill_id && sos.staff_id == staff_id
                    select sos;
            if (x.Any())
            {
                db.staff_of_skill.Remove(x.First());
                db.SaveChanges();
                return Json("Đã xóa thành kỹ năng!", JsonRequestBehavior.AllowGet);
            }
            return Json("Ko co!", JsonRequestBehavior.AllowGet);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
