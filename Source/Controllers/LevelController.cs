﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class LevelController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: Level
        public ActionResult Index()
        {
            return View(_db.levels.ToList());
        }

        // GET: Level/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Level/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Level/Create
        [HttpPost]
        public ActionResult Create(level level)
        {
            if (ModelState.IsValid)
            {
                _db.levels.Add(level);
                _db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Level/Edit/5
        public ActionResult Edit(int id)
        {
            var level = _db.levels.Find(id);
            return View(level);
        }

        // POST: Level/Edit/5
        [HttpPost]
        public ActionResult Edit(level level)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(level).State = EntityState.Modified;
                _db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Level/Delete/5
        public ActionResult Delete(int id)
        {
            var level = _db.levels.Find(id);
            return View(level);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var level = _db.levels.Find(id);

            _db.levels.Remove(level);
            _db.Entry(level).State = EntityState.Deleted;
            _db.SaveChanges();
           
            return View();
        }
    }
}
