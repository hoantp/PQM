﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using QLCLDA.ActionSelector;
using Excel = Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using QLCLDA.Models;
using QLCLDA.Models.Entity;

namespace QLCLDA.Controllers
{
    public class ProjectController : Controller
    {
        private readonly qlcldaEntities _db = new qlcldaEntities();

        // GET: /Project/
        public ActionResult Index()
        {
            var projects = _db.projects.Include(p => p.customer).Include(p => p.end_project).Include(p => p.staff).Include(p => p.quality).Include(p => p.type_project).Include(p => p.type_app);
            return View(projects.ToList());
        }

        // Delete Selected
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string[] foo, int? page)
        {
            var projects = from p in _db.projects
                           select p;

            foreach (var id in foo)
            {
                if (!id.Equals("false"))
                {
                    var project = _db.projects.Find(int.Parse(id));
                    var allTeams = new HashSet<int>(project.teams.Select(t => t.id));
                    foreach (var team in _db.teams.Where(team => allTeams.Contains(team.id)))
                    {
                        project.teams.Remove(team);
                    }
                    _db.projects.Remove(project);
                }
            }
            _db.SaveChanges();

            return View(projects.ToList());
        }

        // Export to Excel
        public ActionResult ToExcelResult(int? id, bank_info bankInfo, string no, string date, string tax, string note)
        {
            if (id != null)
            {
                var newFile = ConvertToExcel(id, bankInfo, no, date, tax, note);

                // Open Dialog Save File
                Response.AddHeader("Content-disposition", "attachment; filename=output.xlsx");
                Response.ContentType = "application/octet-stream";
                Response.TransmitFile(newFile);
                Response.Flush();
                System.IO.File.Delete(newFile);
                Response.End();

                return RedirectToAction("Index");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // Export to PDF
        public ActionResult ToPdfResult(int? id, bank_info bankInfo, string no, string date, string tax, string note)
        {
            if (id != null)
            {
                var appData = Server.MapPath("~/App_Data");
                var newPdf = string.Format("{0}.pdf", Guid.NewGuid());
                var pdfFile = Path.Combine(appData, newPdf);

                var newFile = ConvertToExcel(id, bankInfo, no, date, tax, note);

                Excel._Application excelApplication = new Excel.Application();

                var paramSourceBookPath = newFile;
                var paramMissing = Type.Missing;

                var paramExportFilePath = pdfFile;
                const Excel.XlFixedFormatType paramExportFormat = Excel.XlFixedFormatType.xlTypePDF;
                const Excel.XlFixedFormatQuality paramExportQuality = Excel.XlFixedFormatQuality.xlQualityStandard;
                const bool paramOpenAfterPublish = false;
                const bool paramIncludeDocProps = true;
                const bool paramIgnorePrintAreas = true;
                var paramFromPage = Type.Missing;
                var paramToPage = Type.Missing;

                // Open the source workbook.
                var excelWorkBook = excelApplication.Workbooks.Open(paramSourceBookPath,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing);

                // Save it in the target format.
                if (excelWorkBook != null)
                    excelWorkBook.ExportAsFixedFormat(paramExportFormat,
                        paramExportFilePath, paramExportQuality,
                        paramIncludeDocProps, paramIgnorePrintAreas, paramFromPage,
                        paramToPage, paramOpenAfterPublish,
                        paramMissing);

                // Close the workbook object.
                if (excelWorkBook != null)
                {
                    excelWorkBook.Close(false, paramMissing, paramMissing);
                }

                // Quit Excel and release the ApplicationClass object.
                excelApplication.Quit();

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                // Open Dialog Save File
                Response.AddHeader("Content-disposition", "attachment; filename=output.pdf");
                Response.ContentType = "application/octet-stream";
                Response.TransmitFile(paramExportFilePath);
                Response.Flush();
                System.IO.File.Delete(paramSourceBookPath);
                System.IO.File.Delete(paramExportFilePath);
                Response.End();

                return RedirectToAction("Index");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: /Project/Details/5
        public ActionResult Details(int? id)
        {
            if (id != null)
            {
                var project = _db.projects.Find(id);
                ViewBag.BrSE_Name = _db.staffs.Single(s => s.id == project.brse).name;

                if (project == null)
                {
                    return HttpNotFound();
                }
                return View(project);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: /Project/Create
        public ActionResult Create()
        {
            var project = new project { teams = new List<team>() };
            PopulateAssignedTeamData(project);

            var proLanguage = _db.skills.Where(x => x.type_skill.id == 1).ToList();
            ViewBag.apps = _db.type_app.ToList();
            ViewBag.proLang = proLanguage;
            ViewBag.id_customer = new SelectList(_db.customers, "id", "company");
            //ViewBag.id = new SelectList(_db.end_project, "project_id", "difficulty");
            ViewBag.pm = new SelectList(_db.staffs.Where(x => x.position.id == 1), "id", "name");
            //ViewBag.id = new SelectList(_db.qualities, "project_id", "project_id");
            ViewBag.brse = new SelectList(_db.staffs.Where(x => x.position.id == 2), "id", "name");
            ViewBag.id_type_project = new SelectList(_db.type_project, "id", "name");
            return View();
        }

        // POST: /Project/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_customer,id_type_project,name_cd,name_project,expected_start_date,expected_end_date,actual_start_date,actual_end_date,technology,purpose,estimator,scale_estimator,contract_value,pm,brse,current_cost,estimated_cost,software,hardware")] project project, string[] selectedProLang, string[] selectedApps, string[] selectedTeams)
        {
            var userId = User.Identity.Name;
            if (selectedTeams != null)
            {
                project.teams = new List<team>();
                foreach (var team in selectedTeams)
                {
                    var teamToAdd = _db.teams.Find(int.Parse(team));
                    project.teams.Add(teamToAdd);
                }
            }
            if (selectedProLang != null)
            {
                project.skills = new List<skill>();
                foreach (var pl in selectedProLang)
                {
                    var pro = _db.skills.Find(int.Parse(pl));
                    project.skills.Add(pro);
                }
            }

            if (selectedApps != null)
            {
                project.type_app = new List<type_app>();
                foreach (var app in selectedApps)
                {
                    var ta = _db.type_app.Find(int.Parse(app));
                    project.type_app.Add(ta);
                }
            }

            if (ModelState.IsValid)
            {
                _db.projects.Add(project);
                _db.SaveChanges(userId, project);
                //var activity = new AuditLog(project, UserID);
                return RedirectToAction("Index");
            }

            ViewBag.id_customer = new SelectList(_db.customers, "id", "company", project.id_customer);
            ViewBag.pm = new SelectList(_db.staffs.Where(x => x.position.name == "PM"), "id", "name");
            ViewBag.brse = new SelectList(_db.staffs.Where(x => x.position.name == "BrSE"), "id", "name");
            ViewBag.id_type_project = new SelectList(_db.type_project, "id", "name", project.id_type_project);

            PopulateAssignedTeamData(project);
            return View(project);
        }

        // GET: /Project/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var project = _db.projects.Include(i => i.teams).Include(i => i.type_app).Include(i => i.skills).Single(i => i.id == id);


            if (project == null)
            {
                return HttpNotFound();
            }
            PopulateAssignedTeamData(project);
            PopulateAssignedProgrammingLanguage(project);
            PopulateAssignedApplication(project);

            ViewBag.id_customer = new SelectList(_db.customers, "id", "company", project.id_customer);
            ViewBag.id = new SelectList(_db.end_project, "project_id", "difficulty", project.id);
            ViewBag.pm = new SelectList(_db.staffs.Where(x => x.position.id == 1), "id", "name", project.pm);
            ViewBag.brse = new SelectList(_db.staffs.Where(x => x.position.id == 2), "id", "name", project.brse);
            ViewBag.id_type_project = new SelectList(_db.type_project, "id", "name", project.id_type_project);

            return View(project);
        }

        // POST: /Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string[] selectedProLang, string[] selectedApps, string[] selectedTeams)
        {
            string userId = User.Identity.Name;
            var project = _db.projects.Find(id);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var projectToUpdate = _db.projects.Include(i => i.teams).Single(i => i.id == id);

            if (TryUpdateModel(projectToUpdate, ""))
            {
                try
                {
                    UpdateProjectTeams(selectedTeams, projectToUpdate);
                    UpdateProjectProgrammingLanguage(selectedProLang, projectToUpdate);
                    UpdateProjectApplication(selectedApps, projectToUpdate);
                    //db.Entry(projectToUpdate).State = EntityState.Modified;
                    _db.Entry(project).CurrentValues.SetValues(projectToUpdate);
                    _db.SaveChanges(userId, projectToUpdate);

                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }

            ViewBag.id_customer = new SelectList(_db.customers, "id", "company", projectToUpdate.id_customer);
            ViewBag.id = new SelectList(_db.end_project, "project_id", "difficulty", projectToUpdate.id);
            ViewBag.pm = new SelectList(_db.staffs, "id", "name", projectToUpdate.pm);
            ViewBag.id = new SelectList(_db.qualities, "project_id", "project_id", projectToUpdate.id);
            ViewBag.id_type_project = new SelectList(_db.type_project, "id", "name", projectToUpdate.id_type_project);

            PopulateAssignedTeamData(projectToUpdate);
            return View(projectToUpdate);
        }

        // GET: /Project/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            project project = _db.projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: /Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DeleteItem(id);
            return RedirectToAction("Index");
        }

        private void DeleteItem(int id)
        {
            var project = _db.projects.Find(id);
            var teams = _db.teams.ToList();
            var activities = _db.activity_log.Where(a => a.id_project == id).ToList();

            var allTeams = new HashSet<int>(project.teams.Select(t => t.id));
            foreach (var team in teams)
            {
                if (allTeams.Contains(team.id))
                {
                    project.teams.Remove(team);
                }
            }

            foreach (var act in activities)
            {
                _db.activity_log.Remove(act);
            }

            if (project.end_project != null)
            {
                var endProject = _db.end_project.Find(project.end_project.project_id);
                _db.end_project.Remove(endProject);
            }

            if (project.quality != null)
            {
                var quality = _db.qualities.Find(project.quality.project_id);
                _db.qualities.Remove(quality);
            }

            _db.projects.Remove(project);
            _db.SaveChanges();
        }

        // Lấy danh sách team (truyền qua View Model A.T.D)
        private void PopulateAssignedTeamData(project project)
        {
            var allTeams = _db.teams;
            var projectTeams = new HashSet<int>(project.teams.Select(c => c.id));
            var viewModel = allTeams.Select(team => new AssignedTeamData
            {
                TeamId = team.id,
                Name = team.name,
                Assigned = projectTeams.Contains(team.id)
            }).ToList();
            ViewBag.Teams = viewModel;
        }

        //Lấy danh sách các ngôn ngữ lập trình
        private void PopulateAssignedProgrammingLanguage(project project)
        {
            var proLang = _db.skills.Where(x => x.type_skill.id == 1);
            var projectProLang = new HashSet<int>(project.skills.Select(c => c.id));
            var viewModel = proLang.Select(pl => new AssignedProgrammingLanguage()
            {
                IdSkill = pl.id,
                Name = pl.name,
                Assigned = projectProLang.Contains(pl.id)
            }).ToList();

            ViewBag.proLang = viewModel;
        }

        private void PopulateAssignedApplication(project project)
        {
            var apps = _db.type_app;
            var projectApp = new HashSet<short>(project.type_app.Select(ta => ta.id));
            var viewModel = apps.Select(app => new AssignedApplication()
            {
                IdApp = app.id,
                Name = app.name,
                Assigned = projectApp.Contains(app.id)
            }).ToList();
            ViewBag.apps = viewModel;
        }


        // Update dữ liệu trong bảng Team_Project
        private void UpdateProjectTeams(IEnumerable<string> selectedTeams, project projectToUpdate)
        {
            if (selectedTeams == null)
            {
                projectToUpdate.teams = new List<team>();
                return;
            }

            var selectedTeamsHs = new HashSet<string>(selectedTeams);
            var projectTeams = new HashSet<int>(projectToUpdate.teams.Select(c => c.id));

            foreach (var team in _db.teams)
            {
                if (selectedTeamsHs.Contains(team.id.ToString(CultureInfo.InvariantCulture)))
                {
                    if (!projectTeams.Contains(team.id))
                    {
                        projectToUpdate.teams.Add(team);
                    }
                }
                else
                {
                    if (projectTeams.Contains(team.id))
                    {
                        projectToUpdate.teams.Remove(team);
                    }
                }
            }
        }

        private void UpdateProjectProgrammingLanguage(IEnumerable<string> selectedSkill, project projectToUpdate)
        {
            if (selectedSkill == null)
            {
                projectToUpdate.skills = new List<skill>();
                return;
            }

            var selectedSkillHs = new HashSet<string>(selectedSkill);
            var projectProLang = new HashSet<int>(projectToUpdate.skills.Select(c => c.id));

            foreach (var skill in _db.skills.Where(s => s.type_skill_id == 1))
            {
                if (selectedSkillHs.Contains(skill.id.ToString(CultureInfo.InstalledUICulture)))
                {
                    if (!projectProLang.Contains(skill.id))
                    {
                        projectToUpdate.skills.Add(skill);
                    }

                }
                else
                {
                    if (projectProLang.Contains(skill.id))
                    {
                        projectToUpdate.skills.Remove(skill);
                    }
                }
            }
        }

        private void UpdateProjectApplication(IEnumerable<string> selectedApp, project projectToUpdate)
        {
            if (selectedApp == null)
            {
                projectToUpdate.type_app = new List<type_app>();
                return;
            }
            var selectedAppHs = new HashSet<string>(selectedApp);
            var projectApps = new HashSet<short>(projectToUpdate.type_app.Select(c => c.id));
            foreach (var app in _db.type_app)
            {
                if (selectedAppHs.Contains(app.id.ToString(CultureInfo.InstalledUICulture)))
                {
                    if (!projectApps.Contains(app.id))
                    {
                        projectToUpdate.type_app.Add(app);
                    }
                }
                else
                {
                    if (projectApps.Contains(app.id))
                    {
                        projectToUpdate.type_app.Remove(app);
                    }
                }
            }
        }

        public ActionResult ExportInvoiceDetails()
        {
            ViewBag.idCustomer = new SelectList(_db.customers, "id", "company");
            return View();
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "ExportInvoiceDetails")]
        public ActionResult ExportInvoiceDetails(int idCustomer, string product, string tax, string quantity, string cost, string note)
        {
            var newFile = ConvertToExcelDetails(idCustomer, product, tax, quantity, cost, note);

            // Open Dialog Save File
            Response.AddHeader("Content-disposition", "attachment; filename=output.xlsx");
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(newFile);
            Response.Flush();
            System.IO.File.Delete(newFile);
            Response.End();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "ExportPdfInvoiceDetails")]
        public ActionResult ExportPdfInvoiceDetails(int idCustomer, string product, string tax, string quantity, string cost, string note)
        {
            var appData = Server.MapPath("~/App_Data");
            var newPdf = string.Format("{0}.pdf", Guid.NewGuid());
            var pdfFile = Path.Combine(appData, newPdf);

            var newFile = ConvertToExcelDetails(idCustomer, product, tax, quantity, cost, note);

            Excel._Application excelApplication = new Excel.Application();

            var paramSourceBookPath = newFile;
            var paramMissing = Type.Missing;

            var paramExportFilePath = pdfFile;
            const Excel.XlFixedFormatType paramExportFormat = Excel.XlFixedFormatType.xlTypePDF;
            const Excel.XlFixedFormatQuality paramExportQuality = Excel.XlFixedFormatQuality.xlQualityStandard;
            const bool paramOpenAfterPublish = false;
            const bool paramIncludeDocProps = true;
            const bool paramIgnorePrintAreas = true;
            var paramFromPage = Type.Missing;
            var paramToPage = Type.Missing;

            // Open the source workbook.
            var excelWorkBook = excelApplication.Workbooks.Open(paramSourceBookPath,
                                                                paramMissing, paramMissing, paramMissing, paramMissing,
                                                                paramMissing, paramMissing, paramMissing, paramMissing,
                                                                paramMissing, paramMissing, paramMissing, paramMissing,
                                                                paramMissing, paramMissing);

            // Save it in the target format.
            if (excelWorkBook != null)
                excelWorkBook.ExportAsFixedFormat(paramExportFormat,
                                                  paramExportFilePath, paramExportQuality,
                                                  paramIncludeDocProps, paramIgnorePrintAreas, paramFromPage,
                                                  paramToPage, paramOpenAfterPublish,
                                                  paramMissing);

            // Close the workbook object.
            if (excelWorkBook != null)
            {
                excelWorkBook.Close(false, paramMissing, paramMissing);
            }

            // Quit Excel and release the ApplicationClass object.
            excelApplication.Quit();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            // Open Dialog Save File
            Response.AddHeader("Content-disposition", "attachment; filename=output.pdf");
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(paramExportFilePath);
            Response.Flush();
            System.IO.File.Delete(paramSourceBookPath);
            System.IO.File.Delete(paramExportFilePath);
            Response.End();

            return RedirectToAction("Index");
        }

        // Convert to Excel
        private string ConvertToExcel(int? id, bank_info bankInfo, string no, string date, string tax, string note)
        {
            var appData = Server.MapPath("~/App_Data");
            const string templateFile = "Test.xlsx";
            var newFileName = string.Format("{0}.xlsx", Guid.NewGuid());
            var originFile = Path.Combine(appData, templateFile);
            var newFile = Path.Combine(appData, newFileName);
            var time = DateTime.Now;
            //const string dateInvoice = "　yyyy　年　　M　月　d　日　";
            //const string noInvoice = "　No.     LB.yyyyMM.01";
            const string sheetInvoice = "請求書_yyyyMM";

            var culture = new CultureInfo("en-US", true);
            var DateInvoice = DateTime.Parse(date, culture, DateTimeStyles.AssumeLocal);

            using (var templateXls = new ExcelPackage(new FileInfo(originFile)))
            {
                var template = templateXls.Workbook.Worksheets[1];
                var sheet = templateXls.Workbook.Worksheets.Add(time.ToString(sheetInvoice), template);
                //sheet.Cells["H1"].Value = time.ToString(noInvoice);
                //sheet.Cells["H3"].Value = time.ToString(dateInvoice);
                sheet.Cells["B7"].Value = _db.projects.Single(p => p.id == id).customer.company;
                sheet.Cells["B8"].Value = _db.projects.Single(p => p.id == id).customer.address;
                sheet.Cells["B9"].Value = "TEL: " + _db.projects.Single(p => p.id == id).customer.phone +
                                          "/ FAX: " + _db.projects.Single(p => p.id == id).customer.fax;
                sheet.Cells["B17"].Value = _db.projects.Single(p => p.id == id).name_project;
                sheet.Cells["G17"].Value = _db.projects.Single(p => p.id == id).contract_value;
                sheet.Cells["D21"].Value = bankInfo.bank_pay;
                sheet.Cells["D22"].Value = bankInfo.branch;
                sheet.Cells["D23"].Value = bankInfo.street;
                sheet.Cells["D24"].Value = bankInfo.district;
                sheet.Cells["D25"].Value = "Tel: " + bankInfo.tel + "/Fax: " + bankInfo.fax;
                sheet.Cells["D26"].Value = bankInfo.swift_code;
                sheet.Cells["D27"].Value = bankInfo.account_number;
                sheet.Cells["D28"].Value = bankInfo.name;
                sheet.Cells["J17"].Value = note;
                sheet.Cells["H3"].Value = "　" + DateInvoice.Year + "　年　　" + DateInvoice.Month + "　月　" + DateInvoice.Day + "　日　";
                sheet.Cells["H1"].Value = "　No.     " + no;
                sheet.Cells["H15"].Value = float.Parse(tax) / 100;

                templateXls.Workbook.Worksheets.MoveToStart(2);

                templateXls.SaveAs(new FileInfo(newFile));
            }

            return newFile;
        }

        private string ConvertToExcelDetails(int idCustomer, string product, string tax, string quantity, string cost, string note)
        {
            var appData = Server.MapPath("~/App_Data");
            const string templateFile = "Test2.xlsx";
            var newFileName = string.Format("{0}.xlsx", Guid.NewGuid());
            var originFile = Path.Combine(appData, templateFile);
            var newFile = Path.Combine(appData, newFileName);
            var time = DateTime.Now;
            const string dateInvoice = "　yyyy　年　　M　月　d　日　";
            const string noInvoice = "　No.   ZERO-yyyyMM-1";
            const string sheetInvoice = "請求書_yyyyMM";

            using (var templateXls = new ExcelPackage(new FileInfo(originFile)))
            {
                var template = templateXls.Workbook.Worksheets[1];
                var sheet = templateXls.Workbook.Worksheets.Add(time.ToString(sheetInvoice), template);
                sheet.Cells["H1"].Value = time.ToString(noInvoice);
                sheet.Cells["H3"].Value = time.ToString(dateInvoice);
                sheet.Cells["B7"].Value = _db.customers.Single(c => c.id == idCustomer).company;
                sheet.Cells["B8"].Value = _db.customers.Single(c => c.id == idCustomer).address;
                sheet.Cells["B9"].Value = "Tel." + _db.customers.Single(c => c.id == idCustomer).phone +
                                          " Fax." + _db.customers.Single(c => c.id == idCustomer).fax;
                sheet.Cells["B17"].Value = product;
                sheet.Cells["F17"].Value = double.Parse(quantity);
                sheet.Cells["H17"].Value = double.Parse(cost);
                sheet.Cells["H15"].Value = int.Parse(tax);
                sheet.Cells["J17"].Value = note;
                templateXls.SaveAs(new FileInfo(newFile));
            }

            return newFile;
        }

        public ActionResult Invoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var project = _db.projects.Find(id);
            var bankInfo = _db.bank_info.FirstOrDefault();
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(bankInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Invoice(int? id, [Bind(Include = "id,bank_pay,branch,street,district,tel,fax,swift_code,account_number,name")] bank_info bankInfo, string no, string date, string tax, string note)
        {
            bankInfo.id = 1;
            if (ModelState.IsValid)
            {
                _db.Entry(bankInfo).State = EntityState.Modified;
                _db.SaveChanges();
            }

            ToExcelResult(id, bankInfo, no, date, tax, note);

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
