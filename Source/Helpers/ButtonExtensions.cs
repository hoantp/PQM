﻿using System.Web.Mvc;

namespace QLCLDA.Helpers
{
    public static class ButtonExtensions
    {
        public static MvcHtmlString Button(this HtmlHelper helper, string value, string action)
        {
            var builder = new TagBuilder("input");
            builder.MergeAttribute("type", "Button");
            builder.MergeAttribute("class", "btn btn-default btn_add");
            builder.MergeAttribute("title", value);
            builder.MergeAttribute("value", value);

            var controller = helper.ViewContext.RouteData.GetRequiredString("controller");

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var urlAction = urlHelper.Action(action, controller);

            var url = "location.href='" + urlAction + "'";

            builder.MergeAttribute("onclick", url);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString Button(this HtmlHelper helper, string value, string action, object routeValues)
        {
            var builder = new TagBuilder("input");
            builder.MergeAttribute("type", "Button");
            builder.MergeAttribute("class", "btn btn-default");
            builder.MergeAttribute("title", value);
            builder.MergeAttribute("value", value);

            var controller = helper.ViewContext.RouteData.GetRequiredString("controller");

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var urlAction = urlHelper.Action(action, controller, routeValues);

            var url = "location.href='" + urlAction + "'";

            builder.MergeAttribute("onclick", url);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString Button(this HtmlHelper helper, string value, string action, string controller)
        {
            var builder = new TagBuilder("input");
            builder.MergeAttribute("type", "Button");
            builder.MergeAttribute("class", "btn btn-default");
            builder.MergeAttribute("title", value);
            builder.MergeAttribute("value", value);

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var urlAction = urlHelper.Action(action, controller);

            var url = "location.href='"+ urlAction +"'";

            builder.MergeAttribute("onclick", url);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString Button(this HtmlHelper helper, string value, string action, string controller, object routeValues)
        {
            var builder = new TagBuilder("input");
            builder.MergeAttribute("type", "Button");
            builder.MergeAttribute("class", "btn btn-default");
            builder.MergeAttribute("title", value);
            builder.MergeAttribute("value", value);

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var urlAction = urlHelper.Action(action, controller, routeValues);

            var url = "location.href='" + urlAction + "'";

            builder.MergeAttribute("onclick", url);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
    }
}