﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using QLCLDA.Models.Chart;

namespace QLCLDA.Helpers
{
    public static class ChartValueExtension
    {
        public static MvcHtmlString LineName(this HtmlHelper helper, IEnumerable<ChartValue> value)
        {
            var result = value.Select(item => "'" + item.name + "'").ToList();
            var _name = String.Join(",", result);
            return MvcHtmlString.Create("[" + _name + "]");
        }

        public static MvcHtmlString LineValue(this HtmlHelper helper, IEnumerable<ChartValue> value)
        {
            var result = value.Select(item => item.value.ToString(CultureInfo.InvariantCulture)).ToList();
            var _value = String.Join(",", result);
            return MvcHtmlString.Create("[" + _value + "]");
        }

        public static MvcHtmlString ColumnValue(this HtmlHelper helper, IEnumerable<PieChart> value)
        {
            var result = value.Select(item => "['" + item.name + "'," + item.y + "]").ToList();
            var k = String.Join(",", result);
            return MvcHtmlString.Create("[" + k + "]");
        }
    }
}