
//default
var originalBookingSearchAction;
var originalViewState;

var i = 0;
//MVC300412: Datepicker
function getListMonday(y) {
    var d = new Date(y, 0, 1);

    var sundays = [];
    if (d.getDay() != 1) {
        var beforeDay = d;
        beforeDay.setDate(beforeDay.getDate() - 7);
        sundays.push(new Date(beforeDay));
        d.setDate(d.getDate() + 7);
    }
    while (d.getFullYear() == y) {
        if (d.getDay() === 1) {
            sundays.push(new Date(d));
        }
        d.setDate(d.getDate() + 1);

    }

    return sundays;
}
function updateWeek(y) {
    var weeks = getListMonday(y);

    for (var i = 0; i < 52; i++) {

        $('#start_week').append("<option value=\"" + weeks[i].getDate() + "/" + (weeks[i].getMonth() + 1) + "/" + weeks[i].getFullYear() + "\">" + (i + 1) + "</option>");
        weeks[i].setDate(weeks[i].getDate() + 6);
        if (i == 51) {
            $('#end_week').append("<option value=\"" + weeks[i].getDate() + "/" + (weeks[i].getMonth() + 1) + "/" + weeks[i].getFullYear() + "\" selected=\"selected\">" + (i + 1) + "</option>");
        } else {
            $('#end_week').append("<option value=\"" + weeks[i].getDate() + "/" + (weeks[i].getMonth() + 1) + "/" + weeks[i].getFullYear() + "\">" + (i + 1) + "</option>");
        }
    }
}

function updateStartWeek(y) {
    var weeks = getListMonday(y);
    for (var i = 0; i < 52; i++) {

        $('#start_week').append("<option value=\"" + weeks[i].getDate() + "/" + (weeks[i].getMonth() + 1) + "/" + weeks[i].getFullYear() + "\">" + (i + 1) + "</option>");
    }
}

function updateEndWeek(y) {
    var weeks = getListMonday(y);

    for (var i = 0; i < 52; i++) {
        weeks[i].setDate(weeks[i].getDate() + 6);
        if (i == 51) {
            $('#end_week').append("<option value=\"" + weeks[i].getDate() + "/" + (weeks[i].getMonth() + 1) + "/" + weeks[i].getFullYear() + "\" selected=\"selected\">" + (i + 1) + "</option>");
        } else {
            $('#end_week').append("<option value=\"" + weeks[i].getDate() + "/" + (weeks[i].getMonth() + 1) + "/" + weeks[i].getFullYear() + "\">" + (i + 1) + "</option>");
        }
    }
}


$(function () {

    $("#dateDepart").datepicker({
        showOn: "both",
        buttonImageOnly: false,
        dateFormat: "dd/mm/yy",
        defaultDate: new Date(),
        numberOfMonths: 1,
        changeMonth: true,
        changeYear: true,
        onSelect: function (dateText, inst) {
            //MVC300412: set dynamic return date, should always be equal or greater than depart date
            $("#dateReturn").datepicker("option", "minDate", $("#dateDepart").datepicker("getDate"));



        },
        onClose: function (dateText, inst) {

            var d = new Date($("#dateDepart").val());
            //alert(d.getFullYear);
            $("#choose_year").val(d.getFullYear());
            $("#dateReturn").datepicker("show");

        }
    });

    $("#dateReturn").datepicker({
        showOn: "both",
        buttonImageOnly: false,
        dateFormat: "dd/mm/yy",
        numberOfMonths: 1,
        changeMonth: true,
        changeYear: true,
        //minDate: new Date(),
        // maxDate: "+13m",
        onSelect: function (dateText, inst) {


        }

    });
    $("#dateDepart2").datepicker({
        showOn: "both",
        buttonImageOnly: false,
        dateFormat: "dd/mm/yy",
        defaultDate: new Date(),
        numberOfMonths: 1,
        changeMonth: true,
        changeYear: true,
        onSelect: function (dateText, inst) {
            $("#dateReturn2").datepicker("option", "minDate", $("#dateDepart2").datepicker("getDate"));
        },
        onClose: function (dateText, inst) {
            var d = new Date($("#dateDepart2").val());
            $("#choose_year").val(d.getFullYear());
            $("#dateReturn2").datepicker("show");

        }
    });

    $("#dateReturn2").datepicker({
        showOn: "both",
        buttonImageOnly: false,
        dateFormat: "dd/mm/yy",
        numberOfMonths: 1,
        changeMonth: true,
        changeYear: true
    });

    $('#tabs').tabs({
        activate: function (event, ui) {
            var active = $('#tabs').tabs('option', 'active');
            if (active == 1) {
                new datepickr('start_date', {
                    'dateFormat': 'd/m/Y'
                });
                new datepickr('end_date', {
                    'dateFormat': 'd/m/Y'
                });
            } else if (active == 2) {
                new datepickr('start_date0', {
                    'dateFormat': 'd/m/Y'
                });
                new datepickr('end_date0', {
                    'dateFormat': 'd/m/Y'
                });
            }
        }
    });
    updateWeek(new Date().getFullYear());
    new datepickr('start_date', {
        'dateFormat': 'd/m/Y'
    });
    new datepickr('end_date', {
        'dateFormat': 'd/m/Y'
    });
    new datepickr('start_date0', {
        'dateFormat': 'd/m/Y'
    });
    new datepickr('end_date0', {
        'dateFormat': 'd/m/Y'
    });
});
//init
$(document).ready(function () {

    $("#ui-datepicker-div").on('mouseover', 'td', function () {


        if ($('#dateDepart').val() && $("#MarketStructure").val() != "OneWay" && $('#dateReturn').is(':focus')) {

            $(this).parent().addClass("finalRow");

            //ugly traversing
            var parent = $(this).parent(); //tr
            var parentParent = $(parent).parent(); //tbody
            var parentParentParent = $(parentParent).parent(); //table
            var parentParentParentParent = $(parentParentParent).parent(); //div

            $(".finalRow").prevAll().find("td:not(.ui-datepicker-unselectable)").addClass("highlight");
            $(this).prevAll("td:not(.ui-datepicker-unselectable)").addClass("highlight");

            //check if selecting on last cal
            if ($(parentParentParentParent).hasClass("ui-datepicker-group-last")) {
                $(".ui-datepicker-group-first td:not(.ui-datepicker-unselectable)").addClass("highlight");
            }
        }
    });
    $("#ui-datepicker-div").on('mouseout', 'td', function () {
        $(this).parent().removeClass("finalRow");
        $("#ui-datepicker-div td").removeClass("highlight");
    });




});


function Delete(e) {
    e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);
};

