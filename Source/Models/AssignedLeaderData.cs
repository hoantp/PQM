﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QLCLDA.Models
{
    public class AssignedLeaderData
    {
        [Key]
        public int LeaderId { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}