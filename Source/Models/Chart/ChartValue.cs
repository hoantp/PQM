﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLCLDA.Models.Chart
{
    public class ChartValue
    {
        public string name { get; set; }
        public int value { get; set; }
    }
}