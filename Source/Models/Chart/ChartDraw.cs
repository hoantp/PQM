﻿using System.Collections.Generic;

namespace QLCLDA.Models.Chart
{
    public class ChartDraw
    {
        public ChartDraw()
        {
            this.ProjectCount = new List<ChartValue>();
            this.RevenueTeam = new List<ChartValue>();
            this.IsFailed = new List<ChartValue>();
            this.FailedEachTeam = new List<ChartValue>();
            this.ProfitTeams = new List<ChartValue>();
            this.RevenueYear = new List<ChartValue>();
            this.NumberYear = new List<ChartValue>();
            this.RevenuePersonalTeam = new List<ChartValue>();
            this.SoLuongCharts = new List<PieChart>();
        }
        public string name { get; set; }
        public IList<ChartValue> ProjectCount { get; set; }
        public IList<PieChart> SoLuongCharts { get; set; }  
        public IList<ChartValue> IsFailed { get; set; }
        public IList<ChartValue> RevenueTeam { get; set; }
        public IList<ChartValue> FailedEachTeam { get; set; }
        public IList<ChartValue> ProfitTeams { get; set; }
        public IList<ChartValue> RevenueYear { get; set; }
        public IList<ChartValue> NumberYear { get; set; }

        public IList<ChartValue> RevenuePersonalTeam { get; set; }
    }
}