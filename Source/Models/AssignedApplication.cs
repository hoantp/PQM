﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLCLDA.Models
{
    public class AssignedApplication
    {
        public int IdApp { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}