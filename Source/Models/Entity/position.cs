﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.position")]
    public partial class position
    {
        public position()
        {
            staffs = new HashSet<staff>();
        }

        public short id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Chức vụ")]
        public string name { get; set; }

        [StringLength(200)]
        [Display(Name = "Mô tả")]
        public string description { get; set; }

        public virtual ICollection<staff> staffs { get; set; }
    }
}
