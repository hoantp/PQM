﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.project")]
    public partial class project
    {
        public project()
        {
            activity_log = new HashSet<activity_log>();
            resources = new HashSet<resource>();
            skills = new HashSet<skill>();
            type_app = new HashSet<type_app>();
            teams = new HashSet<team>();
        }

        public int id { get; set; }

        [Display(Name = "Khách hàng (*)")]
        public int id_customer { get; set; }

        [Display(Name = "Loại dự án (*)")]
        public int id_type_project { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Viết tắt (*)")]
        public string name_cd { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Tên dự án (*)")]
        public string name_project { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Ngày bắt đầu (dự kiến) (*)")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime expected_start_date { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Ngày kết thúc (dự kiến) (*)")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime expected_end_date { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Ngày bắt đầu (thực tế)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? actual_start_date { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Ngày kết thúc (thực tế)")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? actual_end_date { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        [Display(Name = "Công nghệ")]
        public string technology { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Mục tiêu dự án (*)")]
        public string purpose { get; set; }

        [StringLength(100)]
        [Display(Name = "Người đánh giá")]
        public string estimator { get; set; }

        [Display(Name = "Quy mô đánh giá (man/month)")]
        public int? scale_estimator { get; set; }

        [Display(Name = "Giá trị hợp đồng")]
        public int? contract_value { get; set; }

        [Display(Name = "PM (*)")]
        public int pm { get; set; }

        [Display(Name = "BrSE")]
        public int? brse { get; set; }

        [Display(Name = "Chi phí hiện tại")]
        public int? current_cost { get; set; }

        [Display(Name = "Chi phí ước tính")]
        public int? estimated_cost { get; set; }

        [StringLength(200)]
        [Display(Name = "Phần mềm")]
        public string software { get; set; }

        [StringLength(200)]
        [Display(Name = "Phần cứng")]
        public string hardware { get; set; }

        public virtual ICollection<activity_log> activity_log { get; set; }

        public virtual customer customer { get; set; }

        public virtual end_project end_project { get; set; }

        public virtual staff staff { get; set; }

        public virtual quality quality { get; set; }

        public virtual type_project type_project { get; set; }

        public virtual staff staff1 { get; set; }

        public virtual staff staff2 { get; set; }

        public virtual ICollection<resource> resources { get; set; }

        public virtual ICollection<skill> skills { get; set; }

        public virtual ICollection<type_app> type_app { get; set; }

        public virtual ICollection<team> teams { get; set; }
    }
}
