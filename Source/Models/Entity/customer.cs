﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.customer")]
    public partial class customer
    {
        public customer()
        {
            projects = new HashSet<project>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Viết tắt (*)")]
        public string company_cd { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Tên Khách hàng (*)")]
        public string company { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Địa chỉ (*)")]
        public string address { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Đại diện (*)")]
        public string representative { get; set; }

        [Required]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email (*)")]
        public string email { get; set; }

        [StringLength(100)]
        [Display(Name = "Skype")]
        public string skype { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Điện thoại (*)")]
        public string phone { get; set; }

        [StringLength(45)]
        [Display(Name = "Ý kiến")]
        public string comments { get; set; }

        [StringLength(100)]
        [Display(Name = "Fax")]
        public string fax { get; set; }

        public virtual ICollection<project> projects { get; set; }
    }
}
