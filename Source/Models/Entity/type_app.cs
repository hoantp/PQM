﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.type_app")]
    public partial class type_app
    {
        public type_app()
        {
            projects = new HashSet<project>();
        }

        public short id { get; set; }

        [Required]
        [StringLength(60)]
        [Display(Name = "Tên loại ứng dụng (*)")]
        public string name { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        [Display(Name = "Mô tả")]
        public string description { get; set; }

        public virtual ICollection<project> projects { get; set; }
    }
}
