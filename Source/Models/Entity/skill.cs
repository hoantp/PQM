﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.skill")]
    public partial class skill
    {
        public skill()
        {
            staff_of_skill = new HashSet<staff_of_skill>();
            projects = new HashSet<project>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Năng lực")]
        public string name { get; set; }

        [Display(Name = "Loại năng lực")]
        public short type_skill_id { get; set; }

        [Display(Name = "Nhóm")]
        public int? parent_skill_id { get; set; }

        public virtual type_skill type_skill { get; set; }

        public virtual ICollection<staff_of_skill> staff_of_skill { get; set; }

        public virtual ICollection<project> projects { get; set; }
    }
}
