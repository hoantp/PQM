namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.resource")]
    public partial class resource
    {
        public resource()
        {
            works = new HashSet<work>();
        }

        public int id { get; set; }

        public int staff_id { get; set; }

        public int project_id { get; set; }

        public virtual project project { get; set; }

        public virtual staff staff { get; set; }

        public virtual ICollection<work> works { get; set; }
    }
}
