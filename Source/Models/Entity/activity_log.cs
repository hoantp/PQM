namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.activity_log")]
    public partial class activity_log
    {
        public int id { get; set; }

        public int id_project { get; set; }

        [Required]
        [StringLength(100)]
        public string who_activity { get; set; }

        public DateTime date_update { get; set; }

        [Column(TypeName = "text")]
        [Required]
        [StringLength(65535)]
        public string content_update { get; set; }

        public virtual project project { get; set; }
    }
}
