﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;

namespace QLCLDA.Models.Entity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class qlcldaEntities : DbContext
    {
        public qlcldaEntities()
            : base("name=qlcldaEntities")
        {
        }

        public virtual DbSet<activity_log> activity_log { get; set; }
        public virtual DbSet<app_estimate> app_estimate { get; set; }
        public virtual DbSet<bank_info> bank_info { get; set; }
        public virtual DbSet<catalogue> catalogues { get; set; }
        public virtual DbSet<customer> customers { get; set; }
        public virtual DbSet<end_project> end_project { get; set; }
        public virtual DbSet<estimate_library> estimate_library { get; set; }
        public virtual DbSet<function_da> function_da { get; set; }
        public virtual DbSet<level> levels { get; set; }
        public virtual DbSet<position> positions { get; set; }
        public virtual DbSet<project> projects { get; set; }
        public virtual DbSet<project_functione> project_functione { get; set; }
        public virtual DbSet<quality> qualities { get; set; }
        public virtual DbSet<resource> resources { get; set; }
        public virtual DbSet<skill> skills { get; set; }
        public virtual DbSet<staff> staffs { get; set; }
        public virtual DbSet<staff_of_skill> staff_of_skill { get; set; }
        public virtual DbSet<task_list> task_list { get; set; }
        public virtual DbSet<task_list_catatogue> task_list_catatogue { get; set; }
        public virtual DbSet<task_list_estimate> task_list_estimate { get; set; }
        public virtual DbSet<team> teams { get; set; }
        public virtual DbSet<team_staff> team_staff { get; set; }
        public virtual DbSet<type_app> type_app { get; set; }
        public virtual DbSet<type_project> type_project { get; set; }
        public virtual DbSet<type_skill> type_skill { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<work> works { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<activity_log>()
                .Property(e => e.who_activity)
                .IsUnicode(false);

            modelBuilder.Entity<activity_log>()
                .Property(e => e.content_update)
                .IsUnicode(false);

            modelBuilder.Entity<app_estimate>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<app_estimate>()
                .Property(e => e.detail)
                .IsUnicode(false);

            modelBuilder.Entity<app_estimate>()
                .Property(e => e.create)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.bank_pay)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.branch)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.street)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.district)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.tel)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.swift_code)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<bank_info>()
                .Property(e => e.account_number)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue>()
                .Property(e => e.Detail)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue>()
                .Property(e => e.create)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.company_cd)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.company)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.representative)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.skype)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.comments)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.projects)
                .WithRequired(e => e.customer)
                .HasForeignKey(e => e.id_customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<end_project>()
                .Property(e => e.difficulty)
                .IsUnicode(false);

            modelBuilder.Entity<end_project>()
                .Property(e => e.advantage)
                .IsUnicode(false);

            modelBuilder.Entity<end_project>()
                .Property(e => e.risk)
                .IsUnicode(false);

            modelBuilder.Entity<end_project>()
                .Property(e => e.experience)
                .IsUnicode(false);

            modelBuilder.Entity<estimate_library>()
                .Property(e => e.external)
                .IsUnicode(false);

            modelBuilder.Entity<estimate_library>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<estimate_library>()
                .Property(e => e.tags)
                .IsUnicode(false);

            modelBuilder.Entity<estimate_library>()
                .Property(e => e.created)
                .IsUnicode(false);

            modelBuilder.Entity<estimate_library>()
                .Property(e => e.mod_date)
                .IsUnicode(false);

            modelBuilder.Entity<function_da>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<function_da>()
                .Property(e => e.detail)
                .IsUnicode(false);

            modelBuilder.Entity<function_da>()
                .Property(e => e.created)
                .IsUnicode(false);

            modelBuilder.Entity<level>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<position>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<position>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<position>()
                .HasMany(e => e.staffs)
                .WithRequired(e => e.position)
                .HasForeignKey(e => e.position_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<project>()
                .Property(e => e.name_cd)
                .IsUnicode(false);

            modelBuilder.Entity<project>()
                .Property(e => e.name_project)
                .IsUnicode(false);

            modelBuilder.Entity<project>()
                .Property(e => e.technology)
                .IsUnicode(false);

            modelBuilder.Entity<project>()
                .Property(e => e.purpose)
                .IsUnicode(false);

            modelBuilder.Entity<project>()
                .Property(e => e.estimator)
                .IsUnicode(false);

            modelBuilder.Entity<project>()
                .Property(e => e.software)
                .IsUnicode(false);

            modelBuilder.Entity<project>()
                .Property(e => e.hardware)
                .IsUnicode(false);

            modelBuilder.Entity<project>()
                .HasMany(e => e.activity_log)
                .WithRequired(e => e.project)
                .HasForeignKey(e => e.id_project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<project>()
                .HasOptional(e => e.end_project)
                .WithRequired(e => e.project);

            modelBuilder.Entity<project>()
                .HasOptional(e => e.quality)
                .WithRequired(e => e.project);

            modelBuilder.Entity<project>()
                .HasMany(e => e.resources)
                .WithRequired(e => e.project)
                .HasForeignKey(e => e.project_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<project>()
                .HasMany(e => e.skills)
                .WithMany(e => e.projects)
                .Map(m => m.ToTable("project_skill", "qlclda"));

            modelBuilder.Entity<project>()
                .HasMany(e => e.type_app)
                .WithMany(e => e.projects)
                .Map(m => m.ToTable("project_type_app", "qlclda"));

            modelBuilder.Entity<resource>()
                .HasMany(e => e.works)
                .WithRequired(e => e.resource)
                .HasForeignKey(e => e.resource_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<skill>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<skill>()
                .HasMany(e => e.staff_of_skill)
                .WithRequired(e => e.skill)
                .HasForeignKey(e => e.skill_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<staff>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<staff>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<staff>()
                .Property(e => e.skype)
                .IsUnicode(false);

            modelBuilder.Entity<staff>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<staff>()
                .HasMany(e => e.projects)
                .WithRequired(e => e.staff)
                .HasForeignKey(e => e.pm)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<staff>()
                .HasMany(e => e.projects1)
                .WithOptional(e => e.staff1)
                .HasForeignKey(e => e.brse);

            modelBuilder.Entity<staff>()
                .HasMany(e => e.projects2)
                .WithOptional(e => e.staff2)
                .HasForeignKey(e => e.brse);

            modelBuilder.Entity<staff>()
                .HasMany(e => e.resources)
                .WithRequired(e => e.staff)
                .HasForeignKey(e => e.staff_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<staff>()
                .HasMany(e => e.staff_of_skill)
                .WithRequired(e => e.staff)
                .HasForeignKey(e => e.staff_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<staff>()
                .HasMany(e => e.team_staff)
                .WithRequired(e => e.staff)
                .HasForeignKey(e => e.id_staff)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<staff_of_skill>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<task_list>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<task_list>()
                .Property(e => e.detail)
                .IsUnicode(false);

            modelBuilder.Entity<task_list>()
                .Property(e => e.created)
                .IsUnicode(false);

            modelBuilder.Entity<task_list_estimate>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<task_list_estimate>()
                .Property(e => e.oss)
                .IsUnicode(false);

            modelBuilder.Entity<task_list_estimate>()
                .Property(e => e.created)
                .IsUnicode(false);

            modelBuilder.Entity<team>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<team>()
                .Property(e => e.workplace)
                .IsUnicode(false);

            modelBuilder.Entity<team>()
                .Property(e => e.technique)
                .IsUnicode(false);

            modelBuilder.Entity<team>()
                .HasMany(e => e.team_staff)
                .WithRequired(e => e.team)
                .HasForeignKey(e => e.id_team)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<team>()
                .HasMany(e => e.projects)
                .WithMany(e => e.teams)
                .Map(m => m.ToTable("team_project", "qlclda").MapLeftKey("id_team").MapRightKey("id_project"));

            modelBuilder.Entity<type_app>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<type_app>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<type_project>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<type_project>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<type_project>()
                .HasMany(e => e.projects)
                .WithRequired(e => e.type_project)
                .HasForeignKey(e => e.id_type_project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<type_skill>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<type_skill>()
                .HasMany(e => e.skills)
                .WithRequired(e => e.type_skill)
                .HasForeignKey(e => e.type_skill_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<user>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.password)
                .IsUnicode(false);
        }

        public int SaveChanges(string userId, project _project)
        {
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
            {
                // For each changed record, get the audit record entries and add them
                foreach (var x in GetAuditRecordsForChange(ent, userId, _project))
                {
                    this.activity_log.Add(x);
                }
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

        private List<activity_log> GetAuditRecordsForChange(DbEntityEntry dbEntry, string userId, project _project)
        {
            var db = new qlcldaEntities();
            var result = new List<activity_log>();
            var changeTime = DateTime.Now;

            switch (dbEntry.State)
            {
                case EntityState.Added:
                    var project_id = 0;
                    result.Add(new activity_log()
                    {
                        who_activity = userId,
                        id_project = project_id,
                        date_update = changeTime,
                        content_update = "[Thêm mới] ID: " + project_id + " - " + _project.name_project
                    });
                    break;
                case EntityState.Deleted:
                    result.Add(new activity_log()
                    {
                        who_activity = userId,
                        id_project = _project.id,
                        date_update = changeTime,
                        content_update = "[Xóa bỏ] ID: " + _project.id + " - " + _project.name_project
                    });
                    break;
                case EntityState.Modified:
                    foreach (var propertyName in dbEntry.OriginalValues.PropertyNames)
                    {
                        // For updates, we only want to capture the columns that actually changed
                        if (!object.Equals(dbEntry.OriginalValues.GetValue<object>(propertyName), dbEntry.CurrentValues.GetValue<object>(propertyName)))
                        {
                            var OriginalValue = dbEntry.OriginalValues.GetValue<object>(propertyName) == null ? null : dbEntry.OriginalValues.GetValue<object>(propertyName).ToString();
                            var NewValue = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString();
                            result.Add(new activity_log()
                            {
                                who_activity = userId,
                                id_project = _project.id,
                                date_update = changeTime,
                                content_update = "[Cập nhật] " + propertyName + ": \"" + OriginalValue + "\" thành \"" + NewValue + "\""
                            });
                        }
                    }
                    break;
            }

            // Otherwise, don't do anything, we don't care about Unchanged or Detached entities
            return result;
        }
    }
}
