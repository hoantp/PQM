namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.catalogue")]
    public partial class catalogue
    {
        public int id { get; set; }

        [Required]
        [StringLength(2000)]
        public string Name { get; set; }

        [Required]
        [StringLength(2000)]
        public string Detail { get; set; }

        [Required]
        [StringLength(200)]
        public string create { get; set; }

        public int is_disable { get; set; }
    }
}
