﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.bank_info")]
    public partial class bank_info
    {
        public int id { get; set; }

        [StringLength(200)]
        [Display(Name = "Ngân hàng trả tiền")]
        public string bank_pay { get; set; }

        [StringLength(200)]
        [Display(Name = "Chi nhánh ngân hàng")]
        public string branch { get; set; }

        [StringLength(200)]
        [Display(Name = "Số nhà, đường")]
        public string street { get; set; }

        [StringLength(200)]
        [Display(Name = "Quận(huyện), Thành phố")]
        public string district { get; set; }

        [StringLength(200)]
        [Display(Name = "Số điện thoại")]
        public string tel { get; set; }

        [StringLength(200)]
        [Display(Name = "Fax")]
        public string fax { get; set; }

        [StringLength(200)]
        [Display(Name = "Swift code")]
        public string swift_code { get; set; }

        [StringLength(200)]
        [Display(Name = "Tên công ty")]
        public string name { get; set; }

        [StringLength(200)]
        [Display(Name = "Số tài khoản")]
        public string account_number { get; set; }
    }
}
