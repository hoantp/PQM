namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.app_estimate")]
    public partial class app_estimate
    {
        public int id { get; set; }

        [Required]
        [StringLength(2000)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string detail { get; set; }

        [Required]
        [StringLength(20)]
        public string create { get; set; }

        public int is_disable { get; set; }
    }
}
