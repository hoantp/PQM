namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.function_da")]
    public partial class function_da
    {
        public int id { get; set; }

        [Required]
        [StringLength(2000)]
        public string name { get; set; }

        [Required]
        [StringLength(2000)]
        public string detail { get; set; }

        [Required]
        [StringLength(20)]
        public string created { get; set; }

        public int order { get; set; }

        public int is_disabled { get; set; }
    }
}
