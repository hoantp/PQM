namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.task_list_catatogue")]
    public partial class task_list_catatogue
    {
        public int id { get; set; }

        public int id_task_list_estimate { get; set; }

        public int id_catalogue { get; set; }

        public int day { get; set; }
    }
}
