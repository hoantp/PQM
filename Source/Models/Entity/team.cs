﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.team")]
    public partial class team
    {
        public team()
        {
            team_staff = new HashSet<team_staff>();
            projects = new HashSet<project>();
            getStaffs = new List<AssignedStaffData>();
            getLeaders = new List<AssignedLeaderData>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Tên (*)")]
        public string name { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Nơi làm việc (*)")]
        public string workplace { get; set; }

        [StringLength(200)]
        [Display(Name = "Công nghệ(*)")]
        public string technique { get; set; }

        [Display(Name = "Thứ tự hiển thị(*)")]
        public short display_order { get; set; }

        public virtual ICollection<team_staff> team_staff { get; set; }

        public virtual ICollection<project> projects { get; set; }

        public IList<AssignedStaffData> getStaffs { get; set; }

        public IList<AssignedLeaderData> getLeaders { get; set; }
    }
}
