﻿using System.Web.Mvc;

namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.end_project")]
    public partial class end_project
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int project_id { get; set; }

        [StringLength(1073741823)]
        [AllowHtml]
        [Display(Name = "Khó khăn")]
        public string difficulty { get; set; }

        [StringLength(1073741823)]
        [AllowHtml]
        [Display(Name = "Thuận lợi")]
        public string advantage { get; set; }

        [StringLength(1073741823)]
        [AllowHtml]
        [Display(Name = "Rủi ro")]
        public string risk { get; set; }

        [StringLength(1073741823)]
        [AllowHtml]
        [Display(Name = "Rủi ro")]
        public string experience { get; set; }

        [Display(Name = "Thất bại?")]
        public bool? is_fail { get; set; }

        public virtual project project { get; set; }
    }
}
