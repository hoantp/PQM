namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.project_functione")]
    public partial class project_functione
    {
        public int id { get; set; }

        public int id_project { get; set; }

        public int id_task_list { get; set; }
    }
}
