namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.works")]
    public partial class work
    {
        public int id { get; set; }

        public int resource_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? work_day { get; set; }

        public int? percent { get; set; }

        public virtual resource resource { get; set; }
    }
}
