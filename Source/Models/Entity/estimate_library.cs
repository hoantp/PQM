namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.estimate_library")]
    public partial class estimate_library
    {
        public int id { get; set; }

        public int id_project { get; set; }

        public int id_type_app { get; set; }

        public int id_type_project { get; set; }

        public int id_function_da { get; set; }

        public int LOC { get; set; }

        [Required]
        [StringLength(2000)]
        public string external { get; set; }

        [Required]
        [StringLength(2000)]
        public string remark { get; set; }

        [Required]
        [StringLength(2000)]
        public string tags { get; set; }

        [Required]
        [StringLength(200)]
        public string created { get; set; }

        [Required]
        [StringLength(200)]
        public string mod_date { get; set; }

        public int order { get; set; }

        public int is_disabled { get; set; }
    }
}
