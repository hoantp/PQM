﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.staff")]
    public partial class staff
    {
        public staff()
        {
            projects = new HashSet<project>();
            projects1 = new HashSet<project>();
            projects2 = new HashSet<project>();
            resources = new HashSet<resource>();
            staff_of_skill = new HashSet<staff_of_skill>();
            team_staff = new HashSet<team_staff>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Tên (*)")]
        public string name { get; set; }

        [StringLength(45)]
        [Display(Name = "Email")]
        public string email { get; set; }

        [StringLength(100)]
        [Display(Name = "Skype")]
        public string skype { get; set; }

        [StringLength(20)]
        [Display(Name = "Điện thoại")]
        public string phone { get; set; }

        [Display(Name = "Vị trí")]
        public short position_id { get; set; }

        [Display(Name = "Sắp xếp")]
        public short? display_order { get; set; }

        [Column(TypeName = "bit")]
        public bool is_deleted { get; set; }

        public virtual position position { get; set; }

        public virtual ICollection<project> projects { get; set; }

        public virtual ICollection<project> projects1 { get; set; }

        public virtual ICollection<project> projects2 { get; set; }

        public virtual ICollection<resource> resources { get; set; }

        public virtual ICollection<staff_of_skill> staff_of_skill { get; set; }

        public virtual ICollection<team_staff> team_staff { get; set; }
    }
}
