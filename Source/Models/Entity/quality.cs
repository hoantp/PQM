﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.quality")]
    public partial class quality
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int project_id { get; set; }

        [Display(Name = "Số dòng code (nghìn dòng)")]
        public int? number_kloc { get; set; }

        [Display(Name = "Số bug tìm được")]
        public int? number_bug_of_team { get; set; }

        [Display(Name = "Số bug/nghìn dòng")]
        public int? number_bug_of_team_in_kloc { get; set; }

        [Display(Name = "Số bug khách hàng gửi")]
        public int? number_bug_of_customer { get; set; }

        [Display(Name = "Số bug khách hàng/nghìn dòng")]
        public int? number_bug_of_customer_int_kloc { get; set; }

        [Display(Name = "Số test case")]
        public int? number_test_case { get; set; }

        [Display(Name = "Nhân lực (người)")]
        public int? human_resource { get; set; }

        [Display(Name = "Số tháng hoàn thành")]
        public int? time_complete { get; set; }

        public virtual project project { get; set; }
    }
}
