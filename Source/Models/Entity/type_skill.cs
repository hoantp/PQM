﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.type_skill")]
    public partial class type_skill
    {
        public type_skill()
        {
            skills = new HashSet<skill>();
        }

        public short id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Loại kỹ năng")]
        public string name { get; set; }

        public virtual ICollection<skill> skills { get; set; }
    }
}
