namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.task_list_estimate")]
    public partial class task_list_estimate
    {
        public int id { get; set; }

        [Required]
        [StringLength(2000)]
        public string name { get; set; }

        public int category { get; set; }

        public int id_app_estimate { get; set; }

        [Required]
        [StringLength(2000)]
        public string oss { get; set; }

        [Required]
        [StringLength(200)]
        public string created { get; set; }

        public int order { get; set; }

        public int is_disabled { get; set; }
    }
}
