﻿namespace QLCLDA.Models.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("qlclda.type_project")]
    public partial class type_project
    {
        public type_project()
        {
            projects = new HashSet<project>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Tên")]
        public string name { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Quốc gia")]
        public string country { get; set; }

        public virtual ICollection<project> projects { get; set; }
    }
}
