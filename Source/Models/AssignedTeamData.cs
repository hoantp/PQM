﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLCLDA.Models
{
    public class AssignedTeamData
    {
        public long TeamId { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}