﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QLCLDA.Models.ExtModel
{
    public class Worker
    {
        public string Name { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? StartDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EndDate { get; set; }

        public int? Percent { get; set; }
    }
}