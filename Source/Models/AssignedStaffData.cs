﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QLCLDA.Models
{
    public class AssignedStaffData
    {
        [Key]
        public int StaffId { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}