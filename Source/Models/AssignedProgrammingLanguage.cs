﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLCLDA.Models
{
    public class AssignedProgrammingLanguage
    {
        public int IdSkill { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}