﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLCLDA.Models
{
    public class AssignedSit
    {
        public int StaffId { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}